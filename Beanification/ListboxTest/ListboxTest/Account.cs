using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

[Serializable]//Makes the class serializable into a data stream file.
public class Account
{
	public int ID;
	public bool isCredit;
	public bool isDebit;
	public string accountName;
	public string trimmedName;
	public int randomKey;
	public string element;
	public bool playerIsCredit;
	public bool playerIsDebit;
	public string playerElement;
	
	public Account( int assignID )
	{
		// require ID to be assigned on creation
		ID = assignID; // set account ID
	}
	
	public string AccountName
	{
		get
		{
			return accountName;
		}
		set
		{
			accountName = value;
		}
	}
	
	public void Trim( string accountName )
	{
		// trim off the first number in account
		trimmedName = accountName.Remove(0,1);
	}
	
	public void randomizeKey()
	{
		// randomize a key and assign it
		Random rnd = new Random();
		
		randomKey = rnd.Next();
	}
	
	public string Element
	{
		get
		{
			return element;
		}
		set
		{
			element = value;
		}
	}
	
	public bool Debit
	{
		get
		{
			return isDebit;
		}
		set
		{
			isDebit = value;
		}
	}
	
	public bool Credit
	{
		get
		{
			return isCredit;
		}
		set
		{
			isCredit = value;
		}
	}
	
	public string PlayerElement
	{
		get
		{
			return playerElement;
		}
		set
		{
			playerElement = value;
		}
	}
	
	public bool PlayerDebit
	{
		get
		{
			return playerIsDebit;
		}
		set
		{
			playerIsDebit = value;
		}
	}
	
	public bool PlayerCredit
	{
		get
		{
			return playerIsCredit;
		}
		set
		{
			playerIsCredit = value;
		}
	}

    public int score()
    {
        int retScore = 0;

       if (element.Equals(playerElement))
       {
           retScore = 1;
       }

        if(isCredit == playerIsCredit && isDebit == playerIsDebit)
        {
            retScore += 1;
        }

       return retScore;
    }
}
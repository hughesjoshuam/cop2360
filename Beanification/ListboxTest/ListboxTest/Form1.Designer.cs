﻿namespace ListboxTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.accountTypePanel20 = new System.Windows.Forms.Panel();
            this.accountTypePanel20Debit = new System.Windows.Forms.Panel();
            this.accountTypePanel20Credit = new System.Windows.Forms.Panel();
            this.listBox21 = new System.Windows.Forms.ListBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.accountTypePanel20Label = new System.Windows.Forms.Label();
            this.accountTypePanel19 = new System.Windows.Forms.Panel();
            this.accountTypePanel19Debit = new System.Windows.Forms.Panel();
            this.accountTypePanel19Credit = new System.Windows.Forms.Panel();
            this.listBox20 = new System.Windows.Forms.ListBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.accountTypePanel19Label = new System.Windows.Forms.Label();
            this.accountTypePanel18 = new System.Windows.Forms.Panel();
            this.accountTypePanel18Debit = new System.Windows.Forms.Panel();
            this.accountTypePanel18Credit = new System.Windows.Forms.Panel();
            this.listBox19 = new System.Windows.Forms.ListBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.accountTypePanel18Label = new System.Windows.Forms.Label();
            this.accountTypePanel17 = new System.Windows.Forms.Panel();
            this.accountTypePanel17Debit = new System.Windows.Forms.Panel();
            this.accountTypePanel17Credit = new System.Windows.Forms.Panel();
            this.listBox18 = new System.Windows.Forms.ListBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.accountTypePanel17Label = new System.Windows.Forms.Label();
            this.accountTypePanel16 = new System.Windows.Forms.Panel();
            this.accountTypePanel16Debit = new System.Windows.Forms.Panel();
            this.accountTypePanel16Credit = new System.Windows.Forms.Panel();
            this.listBox17 = new System.Windows.Forms.ListBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.accountTypePanel16Label = new System.Windows.Forms.Label();
            this.accountTypePanel15 = new System.Windows.Forms.Panel();
            this.accountTypePanel15Debit = new System.Windows.Forms.Panel();
            this.accountTypePanel15Credit = new System.Windows.Forms.Panel();
            this.listBox16 = new System.Windows.Forms.ListBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.accountTypePanel15Label = new System.Windows.Forms.Label();
            this.accountTypePanel14 = new System.Windows.Forms.Panel();
            this.accountTypePanel14Debit = new System.Windows.Forms.Panel();
            this.accountTypePanel14Credit = new System.Windows.Forms.Panel();
            this.listBox15 = new System.Windows.Forms.ListBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.accountTypePanel14Label = new System.Windows.Forms.Label();
            this.accountTypePanel13 = new System.Windows.Forms.Panel();
            this.accountTypePanel13Debit = new System.Windows.Forms.Panel();
            this.accountTypePanel13Credit = new System.Windows.Forms.Panel();
            this.listBox14 = new System.Windows.Forms.ListBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.accountTypePanel13Label = new System.Windows.Forms.Label();
            this.accountTypePanel12 = new System.Windows.Forms.Panel();
            this.accountTypePanel12Debit = new System.Windows.Forms.Panel();
            this.accountTypePanel12Credit = new System.Windows.Forms.Panel();
            this.listBox13 = new System.Windows.Forms.ListBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.accountTypePanel12Label = new System.Windows.Forms.Label();
            this.accountTypePanel11 = new System.Windows.Forms.Panel();
            this.accountTypePanel11Debit = new System.Windows.Forms.Panel();
            this.accountTypePanel11Credit = new System.Windows.Forms.Panel();
            this.listBox12 = new System.Windows.Forms.ListBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.accountTypePanel11Label = new System.Windows.Forms.Label();
            this.accountTypePanel10 = new System.Windows.Forms.Panel();
            this.accountTypePanel10Debit = new System.Windows.Forms.Panel();
            this.accountTypePanel10Credit = new System.Windows.Forms.Panel();
            this.listBox11 = new System.Windows.Forms.ListBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.accountTypePanel10Label = new System.Windows.Forms.Label();
            this.accountTypePanel9 = new System.Windows.Forms.Panel();
            this.accountTypePanel9Debit = new System.Windows.Forms.Panel();
            this.accountTypePanel9Credit = new System.Windows.Forms.Panel();
            this.listBox10 = new System.Windows.Forms.ListBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.accountTypePanel9Label = new System.Windows.Forms.Label();
            this.accountTypePanel8 = new System.Windows.Forms.Panel();
            this.accountTypePanel8Debit = new System.Windows.Forms.Panel();
            this.accountTypePanel8Credit = new System.Windows.Forms.Panel();
            this.listBox9 = new System.Windows.Forms.ListBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.accountTypePanel8Label = new System.Windows.Forms.Label();
            this.accountTypePanel7 = new System.Windows.Forms.Panel();
            this.accountTypePanel7Debit = new System.Windows.Forms.Panel();
            this.accountTypePanel7Credit = new System.Windows.Forms.Panel();
            this.listBox8 = new System.Windows.Forms.ListBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.accountTypePanel7Label = new System.Windows.Forms.Label();
            this.accountTypePanel6 = new System.Windows.Forms.Panel();
            this.accountTypePanel6Debit = new System.Windows.Forms.Panel();
            this.accountTypePanel6Credit = new System.Windows.Forms.Panel();
            this.listBox7 = new System.Windows.Forms.ListBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.accountTypePanel6Label = new System.Windows.Forms.Label();
            this.accountTypePanel5 = new System.Windows.Forms.Panel();
            this.accountTypePanel5Debit = new System.Windows.Forms.Panel();
            this.accountTypePanel5Credit = new System.Windows.Forms.Panel();
            this.listBox6 = new System.Windows.Forms.ListBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.accountTypePanel5Label = new System.Windows.Forms.Label();
            this.accountTypePanel4 = new System.Windows.Forms.Panel();
            this.accountTypePanel4Debit = new System.Windows.Forms.Panel();
            this.accountTypePanel4Credit = new System.Windows.Forms.Panel();
            this.listBox4 = new System.Windows.Forms.ListBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.accountTypePanel4Label = new System.Windows.Forms.Label();
            this.accountTypePanel3 = new System.Windows.Forms.Panel();
            this.accountTypePanel3Debit = new System.Windows.Forms.Panel();
            this.accountTypePanel3Credit = new System.Windows.Forms.Panel();
            this.listBox3 = new System.Windows.Forms.ListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.accountTypePanel3Label = new System.Windows.Forms.Label();
            this.accountTypePanel2 = new System.Windows.Forms.Panel();
            this.accountTypePanel2Debit = new System.Windows.Forms.Panel();
            this.accountTypePanel2Credit = new System.Windows.Forms.Panel();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.accountTypePanel2Label = new System.Windows.Forms.Label();
            this.accountTypePanel1 = new System.Windows.Forms.Panel();
            this.accountTypePanel1Debit = new System.Windows.Forms.Panel();
            this.accountTypePanel1Credit = new System.Windows.Forms.Panel();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.accountTypePanel1Label = new System.Windows.Forms.Label();
            this.listBox5 = new System.Windows.Forms.ListBox();
            this.panel14 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.convertToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.submitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.resetToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.gameObjectiveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.howToPlayGameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.howToChangeAccountSelectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.howToStartGameOverToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scoringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.panel1.SuspendLayout();
            this.accountTypePanel20.SuspendLayout();
            this.accountTypePanel19.SuspendLayout();
            this.accountTypePanel18.SuspendLayout();
            this.accountTypePanel17.SuspendLayout();
            this.accountTypePanel16.SuspendLayout();
            this.accountTypePanel15.SuspendLayout();
            this.accountTypePanel14.SuspendLayout();
            this.accountTypePanel13.SuspendLayout();
            this.accountTypePanel12.SuspendLayout();
            this.accountTypePanel11.SuspendLayout();
            this.accountTypePanel10.SuspendLayout();
            this.accountTypePanel9.SuspendLayout();
            this.accountTypePanel8.SuspendLayout();
            this.accountTypePanel7.SuspendLayout();
            this.accountTypePanel6.SuspendLayout();
            this.accountTypePanel5.SuspendLayout();
            this.accountTypePanel4.SuspendLayout();
            this.accountTypePanel3.SuspendLayout();
            this.accountTypePanel2.SuspendLayout();
            this.accountTypePanel1.SuspendLayout();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.accountTypePanel20);
            this.panel1.Controls.Add(this.accountTypePanel19);
            this.panel1.Controls.Add(this.accountTypePanel18);
            this.panel1.Controls.Add(this.accountTypePanel17);
            this.panel1.Controls.Add(this.accountTypePanel16);
            this.panel1.Controls.Add(this.accountTypePanel15);
            this.panel1.Controls.Add(this.accountTypePanel14);
            this.panel1.Controls.Add(this.accountTypePanel13);
            this.panel1.Controls.Add(this.accountTypePanel12);
            this.panel1.Controls.Add(this.accountTypePanel11);
            this.panel1.Controls.Add(this.accountTypePanel10);
            this.panel1.Controls.Add(this.accountTypePanel9);
            this.panel1.Controls.Add(this.accountTypePanel8);
            this.panel1.Controls.Add(this.accountTypePanel7);
            this.panel1.Controls.Add(this.accountTypePanel6);
            this.panel1.Controls.Add(this.accountTypePanel5);
            this.panel1.Controls.Add(this.accountTypePanel4);
            this.panel1.Controls.Add(this.accountTypePanel3);
            this.panel1.Controls.Add(this.accountTypePanel2);
            this.panel1.Controls.Add(this.accountTypePanel1);
            this.panel1.Location = new System.Drawing.Point(3, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(534, 562);
            this.panel1.TabIndex = 4;
            // 
            // accountTypePanel20
            // 
            this.accountTypePanel20.AllowDrop = true;
            this.accountTypePanel20.AutoSize = true;
            this.accountTypePanel20.Controls.Add(this.accountTypePanel20Debit);
            this.accountTypePanel20.Controls.Add(this.accountTypePanel20Credit);
            this.accountTypePanel20.Controls.Add(this.listBox21);
            this.accountTypePanel20.Controls.Add(this.label58);
            this.accountTypePanel20.Controls.Add(this.label59);
            this.accountTypePanel20.Controls.Add(this.accountTypePanel20Label);
            this.accountTypePanel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.accountTypePanel20.Location = new System.Drawing.Point(0, 2052);
            this.accountTypePanel20.Name = "accountTypePanel20";
            this.accountTypePanel20.Size = new System.Drawing.Size(517, 108);
            this.accountTypePanel20.TabIndex = 19;
            this.accountTypePanel20.Visible = false;
            // 
            // accountTypePanel20Debit
            // 
            this.accountTypePanel20Debit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel20Debit.AutoSize = true;
            this.accountTypePanel20Debit.Location = new System.Drawing.Point(469, 36);
            this.accountTypePanel20Debit.Name = "accountTypePanel20Debit";
            this.accountTypePanel20Debit.Size = new System.Drawing.Size(31, 68);
            this.accountTypePanel20Debit.TabIndex = 5;
            // 
            // accountTypePanel20Credit
            // 
            this.accountTypePanel20Credit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel20Credit.AutoSize = true;
            this.accountTypePanel20Credit.Location = new System.Drawing.Point(418, 36);
            this.accountTypePanel20Credit.Name = "accountTypePanel20Credit";
            this.accountTypePanel20Credit.Size = new System.Drawing.Size(31, 69);
            this.accountTypePanel20Credit.TabIndex = 4;
            // 
            // listBox21
            // 
            this.listBox21.AllowDrop = true;
            this.listBox21.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.listBox21.FormattingEnabled = true;
            this.listBox21.ItemHeight = 24;
            this.listBox21.Location = new System.Drawing.Point(3, 36);
            this.listBox21.Margin = new System.Windows.Forms.Padding(10);
            this.listBox21.Name = "listBox21";
            this.listBox21.Size = new System.Drawing.Size(382, 52);
            this.listBox21.Sorted = true;
            this.listBox21.TabIndex = 1;
            this.listBox21.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox_DragDrop);
            this.listBox21.DragOver += new System.Windows.Forms.DragEventHandler(this.listBox_DragOver);
            this.listBox21.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseDown);
            // 
            // label58
            // 
            this.label58.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label58.Location = new System.Drawing.Point(466, 16);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(41, 17);
            this.label58.TabIndex = 3;
            this.label58.Text = "Debit";
            // 
            // label59
            // 
            this.label59.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label59.Location = new System.Drawing.Point(415, 17);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(45, 17);
            this.label59.TabIndex = 2;
            this.label59.Text = "Credit";
            // 
            // accountTypePanel20Label
            // 
            this.accountTypePanel20Label.AutoSize = true;
            this.accountTypePanel20Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.accountTypePanel20Label.Location = new System.Drawing.Point(3, 17);
            this.accountTypePanel20Label.Name = "accountTypePanel20Label";
            this.accountTypePanel20Label.Size = new System.Drawing.Size(83, 17);
            this.accountTypePanel20Label.TabIndex = 1;
            this.accountTypePanel20Label.Text = "TypeLabel1";
            // 
            // accountTypePanel19
            // 
            this.accountTypePanel19.AllowDrop = true;
            this.accountTypePanel19.AutoSize = true;
            this.accountTypePanel19.Controls.Add(this.accountTypePanel19Debit);
            this.accountTypePanel19.Controls.Add(this.accountTypePanel19Credit);
            this.accountTypePanel19.Controls.Add(this.listBox20);
            this.accountTypePanel19.Controls.Add(this.label55);
            this.accountTypePanel19.Controls.Add(this.label56);
            this.accountTypePanel19.Controls.Add(this.accountTypePanel19Label);
            this.accountTypePanel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.accountTypePanel19.Location = new System.Drawing.Point(0, 1944);
            this.accountTypePanel19.Name = "accountTypePanel19";
            this.accountTypePanel19.Size = new System.Drawing.Size(517, 108);
            this.accountTypePanel19.TabIndex = 18;
            this.accountTypePanel19.Visible = false;
            // 
            // accountTypePanel19Debit
            // 
            this.accountTypePanel19Debit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel19Debit.AutoSize = true;
            this.accountTypePanel19Debit.Location = new System.Drawing.Point(469, 36);
            this.accountTypePanel19Debit.Name = "accountTypePanel19Debit";
            this.accountTypePanel19Debit.Size = new System.Drawing.Size(31, 68);
            this.accountTypePanel19Debit.TabIndex = 5;
            // 
            // accountTypePanel19Credit
            // 
            this.accountTypePanel19Credit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel19Credit.AutoSize = true;
            this.accountTypePanel19Credit.Location = new System.Drawing.Point(418, 36);
            this.accountTypePanel19Credit.Name = "accountTypePanel19Credit";
            this.accountTypePanel19Credit.Size = new System.Drawing.Size(31, 69);
            this.accountTypePanel19Credit.TabIndex = 4;
            // 
            // listBox20
            // 
            this.listBox20.AllowDrop = true;
            this.listBox20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.listBox20.FormattingEnabled = true;
            this.listBox20.ItemHeight = 24;
            this.listBox20.Location = new System.Drawing.Point(3, 36);
            this.listBox20.Margin = new System.Windows.Forms.Padding(10);
            this.listBox20.Name = "listBox20";
            this.listBox20.Size = new System.Drawing.Size(382, 52);
            this.listBox20.Sorted = true;
            this.listBox20.TabIndex = 1;
            this.listBox20.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox_DragDrop);
            this.listBox20.DragOver += new System.Windows.Forms.DragEventHandler(this.listBox_DragOver);
            this.listBox20.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseDown);
            // 
            // label55
            // 
            this.label55.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label55.Location = new System.Drawing.Point(466, 16);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(41, 17);
            this.label55.TabIndex = 3;
            this.label55.Text = "Debit";
            // 
            // label56
            // 
            this.label56.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label56.Location = new System.Drawing.Point(415, 17);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(45, 17);
            this.label56.TabIndex = 2;
            this.label56.Text = "Credit";
            // 
            // accountTypePanel19Label
            // 
            this.accountTypePanel19Label.AutoSize = true;
            this.accountTypePanel19Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.accountTypePanel19Label.Location = new System.Drawing.Point(3, 17);
            this.accountTypePanel19Label.Name = "accountTypePanel19Label";
            this.accountTypePanel19Label.Size = new System.Drawing.Size(83, 17);
            this.accountTypePanel19Label.TabIndex = 1;
            this.accountTypePanel19Label.Text = "TypeLabel1";
            // 
            // accountTypePanel18
            // 
            this.accountTypePanel18.AllowDrop = true;
            this.accountTypePanel18.AutoSize = true;
            this.accountTypePanel18.Controls.Add(this.accountTypePanel18Debit);
            this.accountTypePanel18.Controls.Add(this.accountTypePanel18Credit);
            this.accountTypePanel18.Controls.Add(this.listBox19);
            this.accountTypePanel18.Controls.Add(this.label52);
            this.accountTypePanel18.Controls.Add(this.label53);
            this.accountTypePanel18.Controls.Add(this.accountTypePanel18Label);
            this.accountTypePanel18.Dock = System.Windows.Forms.DockStyle.Top;
            this.accountTypePanel18.Location = new System.Drawing.Point(0, 1836);
            this.accountTypePanel18.Name = "accountTypePanel18";
            this.accountTypePanel18.Size = new System.Drawing.Size(517, 108);
            this.accountTypePanel18.TabIndex = 17;
            this.accountTypePanel18.Visible = false;
            // 
            // accountTypePanel18Debit
            // 
            this.accountTypePanel18Debit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel18Debit.AutoSize = true;
            this.accountTypePanel18Debit.Location = new System.Drawing.Point(469, 36);
            this.accountTypePanel18Debit.Name = "accountTypePanel18Debit";
            this.accountTypePanel18Debit.Size = new System.Drawing.Size(31, 68);
            this.accountTypePanel18Debit.TabIndex = 5;
            // 
            // accountTypePanel18Credit
            // 
            this.accountTypePanel18Credit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel18Credit.AutoSize = true;
            this.accountTypePanel18Credit.Location = new System.Drawing.Point(418, 36);
            this.accountTypePanel18Credit.Name = "accountTypePanel18Credit";
            this.accountTypePanel18Credit.Size = new System.Drawing.Size(31, 69);
            this.accountTypePanel18Credit.TabIndex = 4;
            // 
            // listBox19
            // 
            this.listBox19.AllowDrop = true;
            this.listBox19.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.listBox19.FormattingEnabled = true;
            this.listBox19.ItemHeight = 24;
            this.listBox19.Location = new System.Drawing.Point(3, 36);
            this.listBox19.Margin = new System.Windows.Forms.Padding(10);
            this.listBox19.Name = "listBox19";
            this.listBox19.Size = new System.Drawing.Size(382, 52);
            this.listBox19.Sorted = true;
            this.listBox19.TabIndex = 1;
            this.listBox19.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox_DragDrop);
            this.listBox19.DragOver += new System.Windows.Forms.DragEventHandler(this.listBox_DragOver);
            this.listBox19.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseDown);
            // 
            // label52
            // 
            this.label52.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label52.Location = new System.Drawing.Point(466, 16);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(41, 17);
            this.label52.TabIndex = 3;
            this.label52.Text = "Debit";
            // 
            // label53
            // 
            this.label53.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label53.Location = new System.Drawing.Point(415, 17);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(45, 17);
            this.label53.TabIndex = 2;
            this.label53.Text = "Credit";
            // 
            // accountTypePanel18Label
            // 
            this.accountTypePanel18Label.AutoSize = true;
            this.accountTypePanel18Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.accountTypePanel18Label.Location = new System.Drawing.Point(3, 17);
            this.accountTypePanel18Label.Name = "accountTypePanel18Label";
            this.accountTypePanel18Label.Size = new System.Drawing.Size(83, 17);
            this.accountTypePanel18Label.TabIndex = 1;
            this.accountTypePanel18Label.Text = "TypeLabel1";
            // 
            // accountTypePanel17
            // 
            this.accountTypePanel17.AllowDrop = true;
            this.accountTypePanel17.AutoSize = true;
            this.accountTypePanel17.Controls.Add(this.accountTypePanel17Debit);
            this.accountTypePanel17.Controls.Add(this.accountTypePanel17Credit);
            this.accountTypePanel17.Controls.Add(this.listBox18);
            this.accountTypePanel17.Controls.Add(this.label49);
            this.accountTypePanel17.Controls.Add(this.label50);
            this.accountTypePanel17.Controls.Add(this.accountTypePanel17Label);
            this.accountTypePanel17.Dock = System.Windows.Forms.DockStyle.Top;
            this.accountTypePanel17.Location = new System.Drawing.Point(0, 1728);
            this.accountTypePanel17.Name = "accountTypePanel17";
            this.accountTypePanel17.Size = new System.Drawing.Size(517, 108);
            this.accountTypePanel17.TabIndex = 16;
            this.accountTypePanel17.Visible = false;
            // 
            // accountTypePanel17Debit
            // 
            this.accountTypePanel17Debit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel17Debit.AutoSize = true;
            this.accountTypePanel17Debit.Location = new System.Drawing.Point(469, 36);
            this.accountTypePanel17Debit.Name = "accountTypePanel17Debit";
            this.accountTypePanel17Debit.Size = new System.Drawing.Size(31, 68);
            this.accountTypePanel17Debit.TabIndex = 5;
            // 
            // accountTypePanel17Credit
            // 
            this.accountTypePanel17Credit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel17Credit.AutoSize = true;
            this.accountTypePanel17Credit.Location = new System.Drawing.Point(418, 36);
            this.accountTypePanel17Credit.Name = "accountTypePanel17Credit";
            this.accountTypePanel17Credit.Size = new System.Drawing.Size(31, 69);
            this.accountTypePanel17Credit.TabIndex = 4;
            // 
            // listBox18
            // 
            this.listBox18.AllowDrop = true;
            this.listBox18.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.listBox18.FormattingEnabled = true;
            this.listBox18.ItemHeight = 24;
            this.listBox18.Location = new System.Drawing.Point(3, 36);
            this.listBox18.Margin = new System.Windows.Forms.Padding(10);
            this.listBox18.Name = "listBox18";
            this.listBox18.Size = new System.Drawing.Size(382, 52);
            this.listBox18.Sorted = true;
            this.listBox18.TabIndex = 1;
            this.listBox18.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox_DragDrop);
            this.listBox18.DragOver += new System.Windows.Forms.DragEventHandler(this.listBox_DragOver);
            this.listBox18.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseDown);
            // 
            // label49
            // 
            this.label49.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label49.Location = new System.Drawing.Point(466, 16);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(41, 17);
            this.label49.TabIndex = 3;
            this.label49.Text = "Debit";
            // 
            // label50
            // 
            this.label50.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label50.Location = new System.Drawing.Point(415, 17);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(45, 17);
            this.label50.TabIndex = 2;
            this.label50.Text = "Credit";
            // 
            // accountTypePanel17Label
            // 
            this.accountTypePanel17Label.AutoSize = true;
            this.accountTypePanel17Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.accountTypePanel17Label.Location = new System.Drawing.Point(3, 17);
            this.accountTypePanel17Label.Name = "accountTypePanel17Label";
            this.accountTypePanel17Label.Size = new System.Drawing.Size(83, 17);
            this.accountTypePanel17Label.TabIndex = 1;
            this.accountTypePanel17Label.Text = "TypeLabel1";
            // 
            // accountTypePanel16
            // 
            this.accountTypePanel16.AllowDrop = true;
            this.accountTypePanel16.AutoSize = true;
            this.accountTypePanel16.Controls.Add(this.accountTypePanel16Debit);
            this.accountTypePanel16.Controls.Add(this.accountTypePanel16Credit);
            this.accountTypePanel16.Controls.Add(this.listBox17);
            this.accountTypePanel16.Controls.Add(this.label46);
            this.accountTypePanel16.Controls.Add(this.label47);
            this.accountTypePanel16.Controls.Add(this.accountTypePanel16Label);
            this.accountTypePanel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.accountTypePanel16.Location = new System.Drawing.Point(0, 1620);
            this.accountTypePanel16.Name = "accountTypePanel16";
            this.accountTypePanel16.Size = new System.Drawing.Size(517, 108);
            this.accountTypePanel16.TabIndex = 15;
            this.accountTypePanel16.Visible = false;
            // 
            // accountTypePanel16Debit
            // 
            this.accountTypePanel16Debit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel16Debit.AutoSize = true;
            this.accountTypePanel16Debit.Location = new System.Drawing.Point(469, 36);
            this.accountTypePanel16Debit.Name = "accountTypePanel16Debit";
            this.accountTypePanel16Debit.Size = new System.Drawing.Size(31, 68);
            this.accountTypePanel16Debit.TabIndex = 5;
            // 
            // accountTypePanel16Credit
            // 
            this.accountTypePanel16Credit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel16Credit.AutoSize = true;
            this.accountTypePanel16Credit.Location = new System.Drawing.Point(418, 36);
            this.accountTypePanel16Credit.Name = "accountTypePanel16Credit";
            this.accountTypePanel16Credit.Size = new System.Drawing.Size(31, 69);
            this.accountTypePanel16Credit.TabIndex = 4;
            // 
            // listBox17
            // 
            this.listBox17.AllowDrop = true;
            this.listBox17.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.listBox17.FormattingEnabled = true;
            this.listBox17.ItemHeight = 24;
            this.listBox17.Location = new System.Drawing.Point(3, 36);
            this.listBox17.Margin = new System.Windows.Forms.Padding(10);
            this.listBox17.Name = "listBox17";
            this.listBox17.Size = new System.Drawing.Size(382, 52);
            this.listBox17.Sorted = true;
            this.listBox17.TabIndex = 1;
            this.listBox17.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox_DragDrop);
            this.listBox17.DragOver += new System.Windows.Forms.DragEventHandler(this.listBox_DragOver);
            this.listBox17.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseDown);
            // 
            // label46
            // 
            this.label46.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label46.Location = new System.Drawing.Point(466, 16);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(41, 17);
            this.label46.TabIndex = 3;
            this.label46.Text = "Debit";
            // 
            // label47
            // 
            this.label47.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label47.Location = new System.Drawing.Point(415, 17);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(45, 17);
            this.label47.TabIndex = 2;
            this.label47.Text = "Credit";
            // 
            // accountTypePanel16Label
            // 
            this.accountTypePanel16Label.AutoSize = true;
            this.accountTypePanel16Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.accountTypePanel16Label.Location = new System.Drawing.Point(3, 17);
            this.accountTypePanel16Label.Name = "accountTypePanel16Label";
            this.accountTypePanel16Label.Size = new System.Drawing.Size(83, 17);
            this.accountTypePanel16Label.TabIndex = 1;
            this.accountTypePanel16Label.Text = "TypeLabel1";
            // 
            // accountTypePanel15
            // 
            this.accountTypePanel15.AllowDrop = true;
            this.accountTypePanel15.AutoSize = true;
            this.accountTypePanel15.Controls.Add(this.accountTypePanel15Debit);
            this.accountTypePanel15.Controls.Add(this.accountTypePanel15Credit);
            this.accountTypePanel15.Controls.Add(this.listBox16);
            this.accountTypePanel15.Controls.Add(this.label43);
            this.accountTypePanel15.Controls.Add(this.label44);
            this.accountTypePanel15.Controls.Add(this.accountTypePanel15Label);
            this.accountTypePanel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.accountTypePanel15.Location = new System.Drawing.Point(0, 1512);
            this.accountTypePanel15.Name = "accountTypePanel15";
            this.accountTypePanel15.Size = new System.Drawing.Size(517, 108);
            this.accountTypePanel15.TabIndex = 14;
            this.accountTypePanel15.Visible = false;
            // 
            // accountTypePanel15Debit
            // 
            this.accountTypePanel15Debit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel15Debit.AutoSize = true;
            this.accountTypePanel15Debit.Location = new System.Drawing.Point(469, 36);
            this.accountTypePanel15Debit.Name = "accountTypePanel15Debit";
            this.accountTypePanel15Debit.Size = new System.Drawing.Size(31, 68);
            this.accountTypePanel15Debit.TabIndex = 5;
            // 
            // accountTypePanel15Credit
            // 
            this.accountTypePanel15Credit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel15Credit.AutoSize = true;
            this.accountTypePanel15Credit.Location = new System.Drawing.Point(418, 36);
            this.accountTypePanel15Credit.Name = "accountTypePanel15Credit";
            this.accountTypePanel15Credit.Size = new System.Drawing.Size(31, 69);
            this.accountTypePanel15Credit.TabIndex = 4;
            // 
            // listBox16
            // 
            this.listBox16.AllowDrop = true;
            this.listBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.listBox16.FormattingEnabled = true;
            this.listBox16.ItemHeight = 24;
            this.listBox16.Location = new System.Drawing.Point(3, 36);
            this.listBox16.Margin = new System.Windows.Forms.Padding(10);
            this.listBox16.Name = "listBox16";
            this.listBox16.Size = new System.Drawing.Size(382, 52);
            this.listBox16.Sorted = true;
            this.listBox16.TabIndex = 1;
            this.listBox16.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox_DragDrop);
            this.listBox16.DragOver += new System.Windows.Forms.DragEventHandler(this.listBox_DragOver);
            this.listBox16.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseDown);
            // 
            // label43
            // 
            this.label43.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label43.Location = new System.Drawing.Point(466, 16);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(41, 17);
            this.label43.TabIndex = 3;
            this.label43.Text = "Debit";
            // 
            // label44
            // 
            this.label44.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label44.Location = new System.Drawing.Point(415, 17);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(45, 17);
            this.label44.TabIndex = 2;
            this.label44.Text = "Credit";
            // 
            // accountTypePanel15Label
            // 
            this.accountTypePanel15Label.AutoSize = true;
            this.accountTypePanel15Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.accountTypePanel15Label.Location = new System.Drawing.Point(3, 17);
            this.accountTypePanel15Label.Name = "accountTypePanel15Label";
            this.accountTypePanel15Label.Size = new System.Drawing.Size(83, 17);
            this.accountTypePanel15Label.TabIndex = 1;
            this.accountTypePanel15Label.Text = "TypeLabel1";
            // 
            // accountTypePanel14
            // 
            this.accountTypePanel14.AllowDrop = true;
            this.accountTypePanel14.AutoSize = true;
            this.accountTypePanel14.Controls.Add(this.accountTypePanel14Debit);
            this.accountTypePanel14.Controls.Add(this.accountTypePanel14Credit);
            this.accountTypePanel14.Controls.Add(this.listBox15);
            this.accountTypePanel14.Controls.Add(this.label40);
            this.accountTypePanel14.Controls.Add(this.label41);
            this.accountTypePanel14.Controls.Add(this.accountTypePanel14Label);
            this.accountTypePanel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.accountTypePanel14.Location = new System.Drawing.Point(0, 1404);
            this.accountTypePanel14.Name = "accountTypePanel14";
            this.accountTypePanel14.Size = new System.Drawing.Size(517, 108);
            this.accountTypePanel14.TabIndex = 13;
            this.accountTypePanel14.Visible = false;
            // 
            // accountTypePanel14Debit
            // 
            this.accountTypePanel14Debit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel14Debit.AutoSize = true;
            this.accountTypePanel14Debit.Location = new System.Drawing.Point(469, 36);
            this.accountTypePanel14Debit.Name = "accountTypePanel14Debit";
            this.accountTypePanel14Debit.Size = new System.Drawing.Size(31, 68);
            this.accountTypePanel14Debit.TabIndex = 5;
            // 
            // accountTypePanel14Credit
            // 
            this.accountTypePanel14Credit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel14Credit.AutoSize = true;
            this.accountTypePanel14Credit.Location = new System.Drawing.Point(418, 36);
            this.accountTypePanel14Credit.Name = "accountTypePanel14Credit";
            this.accountTypePanel14Credit.Size = new System.Drawing.Size(31, 69);
            this.accountTypePanel14Credit.TabIndex = 4;
            // 
            // listBox15
            // 
            this.listBox15.AllowDrop = true;
            this.listBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.listBox15.FormattingEnabled = true;
            this.listBox15.ItemHeight = 24;
            this.listBox15.Location = new System.Drawing.Point(3, 36);
            this.listBox15.Margin = new System.Windows.Forms.Padding(10);
            this.listBox15.Name = "listBox15";
            this.listBox15.Size = new System.Drawing.Size(382, 52);
            this.listBox15.Sorted = true;
            this.listBox15.TabIndex = 1;
            this.listBox15.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox_DragDrop);
            this.listBox15.DragOver += new System.Windows.Forms.DragEventHandler(this.listBox_DragOver);
            this.listBox15.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseDown);
            // 
            // label40
            // 
            this.label40.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label40.Location = new System.Drawing.Point(466, 16);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(41, 17);
            this.label40.TabIndex = 3;
            this.label40.Text = "Debit";
            // 
            // label41
            // 
            this.label41.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label41.Location = new System.Drawing.Point(415, 17);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(45, 17);
            this.label41.TabIndex = 2;
            this.label41.Text = "Credit";
            // 
            // accountTypePanel14Label
            // 
            this.accountTypePanel14Label.AutoSize = true;
            this.accountTypePanel14Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.accountTypePanel14Label.Location = new System.Drawing.Point(3, 17);
            this.accountTypePanel14Label.Name = "accountTypePanel14Label";
            this.accountTypePanel14Label.Size = new System.Drawing.Size(83, 17);
            this.accountTypePanel14Label.TabIndex = 1;
            this.accountTypePanel14Label.Text = "TypeLabel1";
            // 
            // accountTypePanel13
            // 
            this.accountTypePanel13.AllowDrop = true;
            this.accountTypePanel13.AutoSize = true;
            this.accountTypePanel13.Controls.Add(this.accountTypePanel13Debit);
            this.accountTypePanel13.Controls.Add(this.accountTypePanel13Credit);
            this.accountTypePanel13.Controls.Add(this.listBox14);
            this.accountTypePanel13.Controls.Add(this.label37);
            this.accountTypePanel13.Controls.Add(this.label38);
            this.accountTypePanel13.Controls.Add(this.accountTypePanel13Label);
            this.accountTypePanel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.accountTypePanel13.Location = new System.Drawing.Point(0, 1296);
            this.accountTypePanel13.Name = "accountTypePanel13";
            this.accountTypePanel13.Size = new System.Drawing.Size(517, 108);
            this.accountTypePanel13.TabIndex = 12;
            this.accountTypePanel13.Visible = false;
            // 
            // accountTypePanel13Debit
            // 
            this.accountTypePanel13Debit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel13Debit.AutoSize = true;
            this.accountTypePanel13Debit.Location = new System.Drawing.Point(469, 36);
            this.accountTypePanel13Debit.Name = "accountTypePanel13Debit";
            this.accountTypePanel13Debit.Size = new System.Drawing.Size(31, 68);
            this.accountTypePanel13Debit.TabIndex = 5;
            // 
            // accountTypePanel13Credit
            // 
            this.accountTypePanel13Credit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel13Credit.AutoSize = true;
            this.accountTypePanel13Credit.Location = new System.Drawing.Point(418, 36);
            this.accountTypePanel13Credit.Name = "accountTypePanel13Credit";
            this.accountTypePanel13Credit.Size = new System.Drawing.Size(31, 69);
            this.accountTypePanel13Credit.TabIndex = 4;
            // 
            // listBox14
            // 
            this.listBox14.AllowDrop = true;
            this.listBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.listBox14.FormattingEnabled = true;
            this.listBox14.ItemHeight = 24;
            this.listBox14.Location = new System.Drawing.Point(3, 36);
            this.listBox14.Margin = new System.Windows.Forms.Padding(10);
            this.listBox14.Name = "listBox14";
            this.listBox14.Size = new System.Drawing.Size(382, 52);
            this.listBox14.Sorted = true;
            this.listBox14.TabIndex = 1;
            this.listBox14.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox_DragDrop);
            this.listBox14.DragOver += new System.Windows.Forms.DragEventHandler(this.listBox_DragOver);
            this.listBox14.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseDown);
            // 
            // label37
            // 
            this.label37.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label37.Location = new System.Drawing.Point(466, 16);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(41, 17);
            this.label37.TabIndex = 3;
            this.label37.Text = "Debit";
            // 
            // label38
            // 
            this.label38.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label38.Location = new System.Drawing.Point(415, 17);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(45, 17);
            this.label38.TabIndex = 2;
            this.label38.Text = "Credit";
            // 
            // accountTypePanel13Label
            // 
            this.accountTypePanel13Label.AutoSize = true;
            this.accountTypePanel13Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.accountTypePanel13Label.Location = new System.Drawing.Point(3, 17);
            this.accountTypePanel13Label.Name = "accountTypePanel13Label";
            this.accountTypePanel13Label.Size = new System.Drawing.Size(83, 17);
            this.accountTypePanel13Label.TabIndex = 1;
            this.accountTypePanel13Label.Text = "TypeLabel1";
            // 
            // accountTypePanel12
            // 
            this.accountTypePanel12.AllowDrop = true;
            this.accountTypePanel12.AutoSize = true;
            this.accountTypePanel12.Controls.Add(this.accountTypePanel12Debit);
            this.accountTypePanel12.Controls.Add(this.accountTypePanel12Credit);
            this.accountTypePanel12.Controls.Add(this.listBox13);
            this.accountTypePanel12.Controls.Add(this.label34);
            this.accountTypePanel12.Controls.Add(this.label35);
            this.accountTypePanel12.Controls.Add(this.accountTypePanel12Label);
            this.accountTypePanel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.accountTypePanel12.Location = new System.Drawing.Point(0, 1188);
            this.accountTypePanel12.Name = "accountTypePanel12";
            this.accountTypePanel12.Size = new System.Drawing.Size(517, 108);
            this.accountTypePanel12.TabIndex = 11;
            this.accountTypePanel12.Visible = false;
            // 
            // accountTypePanel12Debit
            // 
            this.accountTypePanel12Debit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel12Debit.AutoSize = true;
            this.accountTypePanel12Debit.Location = new System.Drawing.Point(469, 36);
            this.accountTypePanel12Debit.Name = "accountTypePanel12Debit";
            this.accountTypePanel12Debit.Size = new System.Drawing.Size(31, 68);
            this.accountTypePanel12Debit.TabIndex = 5;
            // 
            // accountTypePanel12Credit
            // 
            this.accountTypePanel12Credit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel12Credit.AutoSize = true;
            this.accountTypePanel12Credit.Location = new System.Drawing.Point(418, 36);
            this.accountTypePanel12Credit.Name = "accountTypePanel12Credit";
            this.accountTypePanel12Credit.Size = new System.Drawing.Size(31, 69);
            this.accountTypePanel12Credit.TabIndex = 4;
            // 
            // listBox13
            // 
            this.listBox13.AllowDrop = true;
            this.listBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.listBox13.FormattingEnabled = true;
            this.listBox13.ItemHeight = 24;
            this.listBox13.Location = new System.Drawing.Point(3, 36);
            this.listBox13.Margin = new System.Windows.Forms.Padding(10);
            this.listBox13.Name = "listBox13";
            this.listBox13.Size = new System.Drawing.Size(382, 52);
            this.listBox13.Sorted = true;
            this.listBox13.TabIndex = 1;
            this.listBox13.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox_DragDrop);
            this.listBox13.DragOver += new System.Windows.Forms.DragEventHandler(this.listBox_DragOver);
            this.listBox13.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseDown);
            // 
            // label34
            // 
            this.label34.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label34.Location = new System.Drawing.Point(466, 16);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(41, 17);
            this.label34.TabIndex = 3;
            this.label34.Text = "Debit";
            // 
            // label35
            // 
            this.label35.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label35.Location = new System.Drawing.Point(415, 17);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(45, 17);
            this.label35.TabIndex = 2;
            this.label35.Text = "Credit";
            // 
            // accountTypePanel12Label
            // 
            this.accountTypePanel12Label.AutoSize = true;
            this.accountTypePanel12Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.accountTypePanel12Label.Location = new System.Drawing.Point(3, 17);
            this.accountTypePanel12Label.Name = "accountTypePanel12Label";
            this.accountTypePanel12Label.Size = new System.Drawing.Size(83, 17);
            this.accountTypePanel12Label.TabIndex = 1;
            this.accountTypePanel12Label.Text = "TypeLabel1";
            // 
            // accountTypePanel11
            // 
            this.accountTypePanel11.AllowDrop = true;
            this.accountTypePanel11.AutoSize = true;
            this.accountTypePanel11.Controls.Add(this.accountTypePanel11Debit);
            this.accountTypePanel11.Controls.Add(this.accountTypePanel11Credit);
            this.accountTypePanel11.Controls.Add(this.listBox12);
            this.accountTypePanel11.Controls.Add(this.label31);
            this.accountTypePanel11.Controls.Add(this.label32);
            this.accountTypePanel11.Controls.Add(this.accountTypePanel11Label);
            this.accountTypePanel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.accountTypePanel11.Location = new System.Drawing.Point(0, 1080);
            this.accountTypePanel11.Name = "accountTypePanel11";
            this.accountTypePanel11.Size = new System.Drawing.Size(517, 108);
            this.accountTypePanel11.TabIndex = 10;
            this.accountTypePanel11.Visible = false;
            // 
            // accountTypePanel11Debit
            // 
            this.accountTypePanel11Debit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel11Debit.AutoSize = true;
            this.accountTypePanel11Debit.Location = new System.Drawing.Point(469, 36);
            this.accountTypePanel11Debit.Name = "accountTypePanel11Debit";
            this.accountTypePanel11Debit.Size = new System.Drawing.Size(31, 68);
            this.accountTypePanel11Debit.TabIndex = 5;
            // 
            // accountTypePanel11Credit
            // 
            this.accountTypePanel11Credit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel11Credit.AutoSize = true;
            this.accountTypePanel11Credit.Location = new System.Drawing.Point(418, 36);
            this.accountTypePanel11Credit.Name = "accountTypePanel11Credit";
            this.accountTypePanel11Credit.Size = new System.Drawing.Size(31, 69);
            this.accountTypePanel11Credit.TabIndex = 4;
            // 
            // listBox12
            // 
            this.listBox12.AllowDrop = true;
            this.listBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.listBox12.FormattingEnabled = true;
            this.listBox12.ItemHeight = 24;
            this.listBox12.Location = new System.Drawing.Point(3, 36);
            this.listBox12.Margin = new System.Windows.Forms.Padding(10);
            this.listBox12.Name = "listBox12";
            this.listBox12.Size = new System.Drawing.Size(382, 52);
            this.listBox12.Sorted = true;
            this.listBox12.TabIndex = 1;
            this.listBox12.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox_DragDrop);
            this.listBox12.DragOver += new System.Windows.Forms.DragEventHandler(this.listBox_DragOver);
            this.listBox12.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseDown);
            // 
            // label31
            // 
            this.label31.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label31.Location = new System.Drawing.Point(466, 16);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(41, 17);
            this.label31.TabIndex = 3;
            this.label31.Text = "Debit";
            // 
            // label32
            // 
            this.label32.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label32.Location = new System.Drawing.Point(415, 17);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(45, 17);
            this.label32.TabIndex = 2;
            this.label32.Text = "Credit";
            // 
            // accountTypePanel11Label
            // 
            this.accountTypePanel11Label.AutoSize = true;
            this.accountTypePanel11Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.accountTypePanel11Label.Location = new System.Drawing.Point(3, 17);
            this.accountTypePanel11Label.Name = "accountTypePanel11Label";
            this.accountTypePanel11Label.Size = new System.Drawing.Size(83, 17);
            this.accountTypePanel11Label.TabIndex = 1;
            this.accountTypePanel11Label.Text = "TypeLabel1";
            // 
            // accountTypePanel10
            // 
            this.accountTypePanel10.AllowDrop = true;
            this.accountTypePanel10.AutoSize = true;
            this.accountTypePanel10.Controls.Add(this.accountTypePanel10Debit);
            this.accountTypePanel10.Controls.Add(this.accountTypePanel10Credit);
            this.accountTypePanel10.Controls.Add(this.listBox11);
            this.accountTypePanel10.Controls.Add(this.label28);
            this.accountTypePanel10.Controls.Add(this.label29);
            this.accountTypePanel10.Controls.Add(this.accountTypePanel10Label);
            this.accountTypePanel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.accountTypePanel10.Location = new System.Drawing.Point(0, 972);
            this.accountTypePanel10.Name = "accountTypePanel10";
            this.accountTypePanel10.Size = new System.Drawing.Size(517, 108);
            this.accountTypePanel10.TabIndex = 9;
            this.accountTypePanel10.Visible = false;
            // 
            // accountTypePanel10Debit
            // 
            this.accountTypePanel10Debit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel10Debit.AutoSize = true;
            this.accountTypePanel10Debit.Location = new System.Drawing.Point(469, 36);
            this.accountTypePanel10Debit.Name = "accountTypePanel10Debit";
            this.accountTypePanel10Debit.Size = new System.Drawing.Size(31, 68);
            this.accountTypePanel10Debit.TabIndex = 5;
            // 
            // accountTypePanel10Credit
            // 
            this.accountTypePanel10Credit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel10Credit.AutoSize = true;
            this.accountTypePanel10Credit.Location = new System.Drawing.Point(418, 36);
            this.accountTypePanel10Credit.Name = "accountTypePanel10Credit";
            this.accountTypePanel10Credit.Size = new System.Drawing.Size(31, 69);
            this.accountTypePanel10Credit.TabIndex = 4;
            // 
            // listBox11
            // 
            this.listBox11.AllowDrop = true;
            this.listBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.listBox11.FormattingEnabled = true;
            this.listBox11.ItemHeight = 24;
            this.listBox11.Location = new System.Drawing.Point(3, 36);
            this.listBox11.Margin = new System.Windows.Forms.Padding(10);
            this.listBox11.Name = "listBox11";
            this.listBox11.Size = new System.Drawing.Size(382, 52);
            this.listBox11.Sorted = true;
            this.listBox11.TabIndex = 1;
            this.listBox11.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox_DragDrop);
            this.listBox11.DragOver += new System.Windows.Forms.DragEventHandler(this.listBox_DragOver);
            this.listBox11.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseDown);
            // 
            // label28
            // 
            this.label28.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label28.Location = new System.Drawing.Point(466, 16);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(41, 17);
            this.label28.TabIndex = 3;
            this.label28.Text = "Debit";
            // 
            // label29
            // 
            this.label29.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label29.Location = new System.Drawing.Point(415, 17);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(45, 17);
            this.label29.TabIndex = 2;
            this.label29.Text = "Credit";
            // 
            // accountTypePanel10Label
            // 
            this.accountTypePanel10Label.AutoSize = true;
            this.accountTypePanel10Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.accountTypePanel10Label.Location = new System.Drawing.Point(3, 17);
            this.accountTypePanel10Label.Name = "accountTypePanel10Label";
            this.accountTypePanel10Label.Size = new System.Drawing.Size(83, 17);
            this.accountTypePanel10Label.TabIndex = 1;
            this.accountTypePanel10Label.Text = "TypeLabel1";
            // 
            // accountTypePanel9
            // 
            this.accountTypePanel9.AllowDrop = true;
            this.accountTypePanel9.AutoSize = true;
            this.accountTypePanel9.Controls.Add(this.accountTypePanel9Debit);
            this.accountTypePanel9.Controls.Add(this.accountTypePanel9Credit);
            this.accountTypePanel9.Controls.Add(this.listBox10);
            this.accountTypePanel9.Controls.Add(this.label25);
            this.accountTypePanel9.Controls.Add(this.label26);
            this.accountTypePanel9.Controls.Add(this.accountTypePanel9Label);
            this.accountTypePanel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.accountTypePanel9.Location = new System.Drawing.Point(0, 864);
            this.accountTypePanel9.Name = "accountTypePanel9";
            this.accountTypePanel9.Size = new System.Drawing.Size(517, 108);
            this.accountTypePanel9.TabIndex = 8;
            this.accountTypePanel9.Visible = false;
            // 
            // accountTypePanel9Debit
            // 
            this.accountTypePanel9Debit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel9Debit.AutoSize = true;
            this.accountTypePanel9Debit.Location = new System.Drawing.Point(469, 36);
            this.accountTypePanel9Debit.Name = "accountTypePanel9Debit";
            this.accountTypePanel9Debit.Size = new System.Drawing.Size(31, 68);
            this.accountTypePanel9Debit.TabIndex = 5;
            // 
            // accountTypePanel9Credit
            // 
            this.accountTypePanel9Credit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel9Credit.AutoSize = true;
            this.accountTypePanel9Credit.Location = new System.Drawing.Point(418, 36);
            this.accountTypePanel9Credit.Name = "accountTypePanel9Credit";
            this.accountTypePanel9Credit.Size = new System.Drawing.Size(31, 69);
            this.accountTypePanel9Credit.TabIndex = 4;
            // 
            // listBox10
            // 
            this.listBox10.AllowDrop = true;
            this.listBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.listBox10.FormattingEnabled = true;
            this.listBox10.ItemHeight = 24;
            this.listBox10.Location = new System.Drawing.Point(3, 36);
            this.listBox10.Margin = new System.Windows.Forms.Padding(10);
            this.listBox10.Name = "listBox10";
            this.listBox10.Size = new System.Drawing.Size(382, 52);
            this.listBox10.Sorted = true;
            this.listBox10.TabIndex = 1;
            this.listBox10.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox_DragDrop);
            this.listBox10.DragOver += new System.Windows.Forms.DragEventHandler(this.listBox_DragOver);
            this.listBox10.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseDown);
            // 
            // label25
            // 
            this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label25.Location = new System.Drawing.Point(466, 16);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(41, 17);
            this.label25.TabIndex = 3;
            this.label25.Text = "Debit";
            // 
            // label26
            // 
            this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label26.Location = new System.Drawing.Point(415, 17);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(45, 17);
            this.label26.TabIndex = 2;
            this.label26.Text = "Credit";
            // 
            // accountTypePanel9Label
            // 
            this.accountTypePanel9Label.AutoSize = true;
            this.accountTypePanel9Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.accountTypePanel9Label.Location = new System.Drawing.Point(3, 17);
            this.accountTypePanel9Label.Name = "accountTypePanel9Label";
            this.accountTypePanel9Label.Size = new System.Drawing.Size(83, 17);
            this.accountTypePanel9Label.TabIndex = 1;
            this.accountTypePanel9Label.Text = "TypeLabel1";
            // 
            // accountTypePanel8
            // 
            this.accountTypePanel8.AllowDrop = true;
            this.accountTypePanel8.AutoSize = true;
            this.accountTypePanel8.Controls.Add(this.accountTypePanel8Debit);
            this.accountTypePanel8.Controls.Add(this.accountTypePanel8Credit);
            this.accountTypePanel8.Controls.Add(this.listBox9);
            this.accountTypePanel8.Controls.Add(this.label22);
            this.accountTypePanel8.Controls.Add(this.label23);
            this.accountTypePanel8.Controls.Add(this.accountTypePanel8Label);
            this.accountTypePanel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.accountTypePanel8.Location = new System.Drawing.Point(0, 756);
            this.accountTypePanel8.Name = "accountTypePanel8";
            this.accountTypePanel8.Size = new System.Drawing.Size(517, 108);
            this.accountTypePanel8.TabIndex = 7;
            this.accountTypePanel8.Visible = false;
            // 
            // accountTypePanel8Debit
            // 
            this.accountTypePanel8Debit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel8Debit.AutoSize = true;
            this.accountTypePanel8Debit.Location = new System.Drawing.Point(469, 36);
            this.accountTypePanel8Debit.Name = "accountTypePanel8Debit";
            this.accountTypePanel8Debit.Size = new System.Drawing.Size(31, 68);
            this.accountTypePanel8Debit.TabIndex = 5;
            // 
            // accountTypePanel8Credit
            // 
            this.accountTypePanel8Credit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel8Credit.AutoSize = true;
            this.accountTypePanel8Credit.Location = new System.Drawing.Point(418, 36);
            this.accountTypePanel8Credit.Name = "accountTypePanel8Credit";
            this.accountTypePanel8Credit.Size = new System.Drawing.Size(31, 69);
            this.accountTypePanel8Credit.TabIndex = 4;
            // 
            // listBox9
            // 
            this.listBox9.AllowDrop = true;
            this.listBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.listBox9.FormattingEnabled = true;
            this.listBox9.ItemHeight = 24;
            this.listBox9.Location = new System.Drawing.Point(3, 36);
            this.listBox9.Margin = new System.Windows.Forms.Padding(10);
            this.listBox9.Name = "listBox9";
            this.listBox9.Size = new System.Drawing.Size(382, 52);
            this.listBox9.Sorted = true;
            this.listBox9.TabIndex = 1;
            this.listBox9.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox_DragDrop);
            this.listBox9.DragOver += new System.Windows.Forms.DragEventHandler(this.listBox_DragOver);
            this.listBox9.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseDown);
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label22.Location = new System.Drawing.Point(466, 16);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 17);
            this.label22.TabIndex = 3;
            this.label22.Text = "Debit";
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label23.Location = new System.Drawing.Point(415, 17);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(45, 17);
            this.label23.TabIndex = 2;
            this.label23.Text = "Credit";
            // 
            // accountTypePanel8Label
            // 
            this.accountTypePanel8Label.AutoSize = true;
            this.accountTypePanel8Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.accountTypePanel8Label.Location = new System.Drawing.Point(3, 17);
            this.accountTypePanel8Label.Name = "accountTypePanel8Label";
            this.accountTypePanel8Label.Size = new System.Drawing.Size(83, 17);
            this.accountTypePanel8Label.TabIndex = 1;
            this.accountTypePanel8Label.Text = "TypeLabel1";
            // 
            // accountTypePanel7
            // 
            this.accountTypePanel7.AllowDrop = true;
            this.accountTypePanel7.AutoSize = true;
            this.accountTypePanel7.Controls.Add(this.accountTypePanel7Debit);
            this.accountTypePanel7.Controls.Add(this.accountTypePanel7Credit);
            this.accountTypePanel7.Controls.Add(this.listBox8);
            this.accountTypePanel7.Controls.Add(this.label19);
            this.accountTypePanel7.Controls.Add(this.label20);
            this.accountTypePanel7.Controls.Add(this.accountTypePanel7Label);
            this.accountTypePanel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.accountTypePanel7.Location = new System.Drawing.Point(0, 648);
            this.accountTypePanel7.Name = "accountTypePanel7";
            this.accountTypePanel7.Size = new System.Drawing.Size(517, 108);
            this.accountTypePanel7.TabIndex = 6;
            this.accountTypePanel7.Visible = false;
            // 
            // accountTypePanel7Debit
            // 
            this.accountTypePanel7Debit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel7Debit.AutoSize = true;
            this.accountTypePanel7Debit.Location = new System.Drawing.Point(469, 36);
            this.accountTypePanel7Debit.Name = "accountTypePanel7Debit";
            this.accountTypePanel7Debit.Size = new System.Drawing.Size(31, 68);
            this.accountTypePanel7Debit.TabIndex = 5;
            // 
            // accountTypePanel7Credit
            // 
            this.accountTypePanel7Credit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel7Credit.AutoSize = true;
            this.accountTypePanel7Credit.Location = new System.Drawing.Point(418, 36);
            this.accountTypePanel7Credit.Name = "accountTypePanel7Credit";
            this.accountTypePanel7Credit.Size = new System.Drawing.Size(31, 69);
            this.accountTypePanel7Credit.TabIndex = 4;
            // 
            // listBox8
            // 
            this.listBox8.AllowDrop = true;
            this.listBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.listBox8.FormattingEnabled = true;
            this.listBox8.ItemHeight = 24;
            this.listBox8.Location = new System.Drawing.Point(3, 36);
            this.listBox8.Margin = new System.Windows.Forms.Padding(10);
            this.listBox8.Name = "listBox8";
            this.listBox8.Size = new System.Drawing.Size(382, 52);
            this.listBox8.Sorted = true;
            this.listBox8.TabIndex = 1;
            this.listBox8.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox_DragDrop);
            this.listBox8.DragOver += new System.Windows.Forms.DragEventHandler(this.listBox_DragOver);
            this.listBox8.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseDown);
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label19.Location = new System.Drawing.Point(466, 16);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(41, 17);
            this.label19.TabIndex = 3;
            this.label19.Text = "Debit";
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label20.Location = new System.Drawing.Point(415, 17);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(45, 17);
            this.label20.TabIndex = 2;
            this.label20.Text = "Credit";
            // 
            // accountTypePanel7Label
            // 
            this.accountTypePanel7Label.AutoSize = true;
            this.accountTypePanel7Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.accountTypePanel7Label.Location = new System.Drawing.Point(3, 17);
            this.accountTypePanel7Label.Name = "accountTypePanel7Label";
            this.accountTypePanel7Label.Size = new System.Drawing.Size(83, 17);
            this.accountTypePanel7Label.TabIndex = 1;
            this.accountTypePanel7Label.Text = "TypeLabel1";
            // 
            // accountTypePanel6
            // 
            this.accountTypePanel6.AllowDrop = true;
            this.accountTypePanel6.AutoSize = true;
            this.accountTypePanel6.Controls.Add(this.accountTypePanel6Debit);
            this.accountTypePanel6.Controls.Add(this.accountTypePanel6Credit);
            this.accountTypePanel6.Controls.Add(this.listBox7);
            this.accountTypePanel6.Controls.Add(this.label16);
            this.accountTypePanel6.Controls.Add(this.label17);
            this.accountTypePanel6.Controls.Add(this.accountTypePanel6Label);
            this.accountTypePanel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.accountTypePanel6.Location = new System.Drawing.Point(0, 540);
            this.accountTypePanel6.Name = "accountTypePanel6";
            this.accountTypePanel6.Size = new System.Drawing.Size(517, 108);
            this.accountTypePanel6.TabIndex = 5;
            this.accountTypePanel6.Visible = false;
            // 
            // accountTypePanel6Debit
            // 
            this.accountTypePanel6Debit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel6Debit.AutoSize = true;
            this.accountTypePanel6Debit.Location = new System.Drawing.Point(469, 36);
            this.accountTypePanel6Debit.Name = "accountTypePanel6Debit";
            this.accountTypePanel6Debit.Size = new System.Drawing.Size(31, 68);
            this.accountTypePanel6Debit.TabIndex = 5;
            // 
            // accountTypePanel6Credit
            // 
            this.accountTypePanel6Credit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel6Credit.AutoSize = true;
            this.accountTypePanel6Credit.Location = new System.Drawing.Point(418, 36);
            this.accountTypePanel6Credit.Name = "accountTypePanel6Credit";
            this.accountTypePanel6Credit.Size = new System.Drawing.Size(31, 69);
            this.accountTypePanel6Credit.TabIndex = 4;
            // 
            // listBox7
            // 
            this.listBox7.AllowDrop = true;
            this.listBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.listBox7.FormattingEnabled = true;
            this.listBox7.ItemHeight = 24;
            this.listBox7.Location = new System.Drawing.Point(3, 36);
            this.listBox7.Margin = new System.Windows.Forms.Padding(10);
            this.listBox7.Name = "listBox7";
            this.listBox7.Size = new System.Drawing.Size(382, 52);
            this.listBox7.Sorted = true;
            this.listBox7.TabIndex = 1;
            this.listBox7.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox_DragDrop);
            this.listBox7.DragOver += new System.Windows.Forms.DragEventHandler(this.listBox_DragOver);
            this.listBox7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseDown);
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label16.Location = new System.Drawing.Point(466, 16);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 17);
            this.label16.TabIndex = 3;
            this.label16.Text = "Debit";
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label17.Location = new System.Drawing.Point(415, 17);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(45, 17);
            this.label17.TabIndex = 2;
            this.label17.Text = "Credit";
            // 
            // accountTypePanel6Label
            // 
            this.accountTypePanel6Label.AutoSize = true;
            this.accountTypePanel6Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.accountTypePanel6Label.Location = new System.Drawing.Point(3, 17);
            this.accountTypePanel6Label.Name = "accountTypePanel6Label";
            this.accountTypePanel6Label.Size = new System.Drawing.Size(83, 17);
            this.accountTypePanel6Label.TabIndex = 1;
            this.accountTypePanel6Label.Text = "TypeLabel1";
            // 
            // accountTypePanel5
            // 
            this.accountTypePanel5.AllowDrop = true;
            this.accountTypePanel5.AutoSize = true;
            this.accountTypePanel5.Controls.Add(this.accountTypePanel5Debit);
            this.accountTypePanel5.Controls.Add(this.accountTypePanel5Credit);
            this.accountTypePanel5.Controls.Add(this.listBox6);
            this.accountTypePanel5.Controls.Add(this.label13);
            this.accountTypePanel5.Controls.Add(this.label14);
            this.accountTypePanel5.Controls.Add(this.accountTypePanel5Label);
            this.accountTypePanel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.accountTypePanel5.Location = new System.Drawing.Point(0, 432);
            this.accountTypePanel5.Name = "accountTypePanel5";
            this.accountTypePanel5.Size = new System.Drawing.Size(517, 108);
            this.accountTypePanel5.TabIndex = 4;
            this.accountTypePanel5.Visible = false;
            // 
            // accountTypePanel5Debit
            // 
            this.accountTypePanel5Debit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel5Debit.AutoSize = true;
            this.accountTypePanel5Debit.Location = new System.Drawing.Point(469, 36);
            this.accountTypePanel5Debit.Name = "accountTypePanel5Debit";
            this.accountTypePanel5Debit.Size = new System.Drawing.Size(31, 68);
            this.accountTypePanel5Debit.TabIndex = 5;
            // 
            // accountTypePanel5Credit
            // 
            this.accountTypePanel5Credit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel5Credit.AutoSize = true;
            this.accountTypePanel5Credit.Location = new System.Drawing.Point(418, 36);
            this.accountTypePanel5Credit.Name = "accountTypePanel5Credit";
            this.accountTypePanel5Credit.Size = new System.Drawing.Size(31, 69);
            this.accountTypePanel5Credit.TabIndex = 4;
            // 
            // listBox6
            // 
            this.listBox6.AllowDrop = true;
            this.listBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.listBox6.FormattingEnabled = true;
            this.listBox6.ItemHeight = 24;
            this.listBox6.Location = new System.Drawing.Point(3, 36);
            this.listBox6.Margin = new System.Windows.Forms.Padding(10);
            this.listBox6.Name = "listBox6";
            this.listBox6.Size = new System.Drawing.Size(382, 52);
            this.listBox6.Sorted = true;
            this.listBox6.TabIndex = 1;
            this.listBox6.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox_DragDrop);
            this.listBox6.DragOver += new System.Windows.Forms.DragEventHandler(this.listBox_DragOver);
            this.listBox6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseDown);
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label13.Location = new System.Drawing.Point(466, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 17);
            this.label13.TabIndex = 3;
            this.label13.Text = "Debit";
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label14.Location = new System.Drawing.Point(415, 17);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 17);
            this.label14.TabIndex = 2;
            this.label14.Text = "Credit";
            // 
            // accountTypePanel5Label
            // 
            this.accountTypePanel5Label.AutoSize = true;
            this.accountTypePanel5Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.accountTypePanel5Label.Location = new System.Drawing.Point(3, 17);
            this.accountTypePanel5Label.Name = "accountTypePanel5Label";
            this.accountTypePanel5Label.Size = new System.Drawing.Size(83, 17);
            this.accountTypePanel5Label.TabIndex = 1;
            this.accountTypePanel5Label.Text = "TypeLabel1";
            // 
            // accountTypePanel4
            // 
            this.accountTypePanel4.AllowDrop = true;
            this.accountTypePanel4.AutoSize = true;
            this.accountTypePanel4.Controls.Add(this.accountTypePanel4Debit);
            this.accountTypePanel4.Controls.Add(this.accountTypePanel4Credit);
            this.accountTypePanel4.Controls.Add(this.listBox4);
            this.accountTypePanel4.Controls.Add(this.label10);
            this.accountTypePanel4.Controls.Add(this.label11);
            this.accountTypePanel4.Controls.Add(this.accountTypePanel4Label);
            this.accountTypePanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.accountTypePanel4.Location = new System.Drawing.Point(0, 324);
            this.accountTypePanel4.Name = "accountTypePanel4";
            this.accountTypePanel4.Size = new System.Drawing.Size(517, 108);
            this.accountTypePanel4.TabIndex = 3;
            this.accountTypePanel4.Visible = false;
            // 
            // accountTypePanel4Debit
            // 
            this.accountTypePanel4Debit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel4Debit.AutoSize = true;
            this.accountTypePanel4Debit.Location = new System.Drawing.Point(469, 36);
            this.accountTypePanel4Debit.Name = "accountTypePanel4Debit";
            this.accountTypePanel4Debit.Size = new System.Drawing.Size(31, 68);
            this.accountTypePanel4Debit.TabIndex = 5;
            // 
            // accountTypePanel4Credit
            // 
            this.accountTypePanel4Credit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel4Credit.AutoSize = true;
            this.accountTypePanel4Credit.Location = new System.Drawing.Point(418, 36);
            this.accountTypePanel4Credit.Name = "accountTypePanel4Credit";
            this.accountTypePanel4Credit.Size = new System.Drawing.Size(31, 69);
            this.accountTypePanel4Credit.TabIndex = 4;
            // 
            // listBox4
            // 
            this.listBox4.AllowDrop = true;
            this.listBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.listBox4.FormattingEnabled = true;
            this.listBox4.ItemHeight = 24;
            this.listBox4.Location = new System.Drawing.Point(3, 36);
            this.listBox4.Margin = new System.Windows.Forms.Padding(10);
            this.listBox4.Name = "listBox4";
            this.listBox4.Size = new System.Drawing.Size(382, 52);
            this.listBox4.TabIndex = 1;
            this.listBox4.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox_DragDrop);
            this.listBox4.DragOver += new System.Windows.Forms.DragEventHandler(this.listBox_DragOver);
            this.listBox4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseDown);
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label10.Location = new System.Drawing.Point(466, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 17);
            this.label10.TabIndex = 3;
            this.label10.Text = "Debit";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label11.Location = new System.Drawing.Point(415, 17);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 17);
            this.label11.TabIndex = 2;
            this.label11.Text = "Credit";
            // 
            // accountTypePanel4Label
            // 
            this.accountTypePanel4Label.AutoSize = true;
            this.accountTypePanel4Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.accountTypePanel4Label.Location = new System.Drawing.Point(3, 17);
            this.accountTypePanel4Label.Name = "accountTypePanel4Label";
            this.accountTypePanel4Label.Size = new System.Drawing.Size(83, 17);
            this.accountTypePanel4Label.TabIndex = 1;
            this.accountTypePanel4Label.Text = "TypeLabel4";
            // 
            // accountTypePanel3
            // 
            this.accountTypePanel3.AllowDrop = true;
            this.accountTypePanel3.AutoSize = true;
            this.accountTypePanel3.Controls.Add(this.accountTypePanel3Debit);
            this.accountTypePanel3.Controls.Add(this.accountTypePanel3Credit);
            this.accountTypePanel3.Controls.Add(this.listBox3);
            this.accountTypePanel3.Controls.Add(this.label7);
            this.accountTypePanel3.Controls.Add(this.label8);
            this.accountTypePanel3.Controls.Add(this.accountTypePanel3Label);
            this.accountTypePanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.accountTypePanel3.Location = new System.Drawing.Point(0, 216);
            this.accountTypePanel3.Name = "accountTypePanel3";
            this.accountTypePanel3.Size = new System.Drawing.Size(517, 108);
            this.accountTypePanel3.TabIndex = 2;
            this.accountTypePanel3.Visible = false;
            // 
            // accountTypePanel3Debit
            // 
            this.accountTypePanel3Debit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel3Debit.AutoSize = true;
            this.accountTypePanel3Debit.Location = new System.Drawing.Point(469, 36);
            this.accountTypePanel3Debit.Name = "accountTypePanel3Debit";
            this.accountTypePanel3Debit.Size = new System.Drawing.Size(31, 68);
            this.accountTypePanel3Debit.TabIndex = 5;
            // 
            // accountTypePanel3Credit
            // 
            this.accountTypePanel3Credit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel3Credit.AutoSize = true;
            this.accountTypePanel3Credit.Location = new System.Drawing.Point(418, 36);
            this.accountTypePanel3Credit.Name = "accountTypePanel3Credit";
            this.accountTypePanel3Credit.Size = new System.Drawing.Size(31, 69);
            this.accountTypePanel3Credit.TabIndex = 4;
            // 
            // listBox3
            // 
            this.listBox3.AllowDrop = true;
            this.listBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.listBox3.FormattingEnabled = true;
            this.listBox3.ItemHeight = 24;
            this.listBox3.Location = new System.Drawing.Point(3, 36);
            this.listBox3.Margin = new System.Windows.Forms.Padding(10);
            this.listBox3.Name = "listBox3";
            this.listBox3.Size = new System.Drawing.Size(382, 52);
            this.listBox3.TabIndex = 1;
            this.listBox3.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox_DragDrop);
            this.listBox3.DragOver += new System.Windows.Forms.DragEventHandler(this.listBox_DragOver);
            this.listBox3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseDown);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label7.Location = new System.Drawing.Point(466, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 17);
            this.label7.TabIndex = 3;
            this.label7.Text = "Debit";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label8.Location = new System.Drawing.Point(415, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 17);
            this.label8.TabIndex = 2;
            this.label8.Text = "Credit";
            // 
            // accountTypePanel3Label
            // 
            this.accountTypePanel3Label.AutoSize = true;
            this.accountTypePanel3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.accountTypePanel3Label.Location = new System.Drawing.Point(3, 17);
            this.accountTypePanel3Label.Name = "accountTypePanel3Label";
            this.accountTypePanel3Label.Size = new System.Drawing.Size(83, 17);
            this.accountTypePanel3Label.TabIndex = 1;
            this.accountTypePanel3Label.Text = "TypeLabel3";
            // 
            // accountTypePanel2
            // 
            this.accountTypePanel2.AllowDrop = true;
            this.accountTypePanel2.AutoSize = true;
            this.accountTypePanel2.Controls.Add(this.accountTypePanel2Debit);
            this.accountTypePanel2.Controls.Add(this.accountTypePanel2Credit);
            this.accountTypePanel2.Controls.Add(this.listBox2);
            this.accountTypePanel2.Controls.Add(this.label4);
            this.accountTypePanel2.Controls.Add(this.label5);
            this.accountTypePanel2.Controls.Add(this.accountTypePanel2Label);
            this.accountTypePanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.accountTypePanel2.Location = new System.Drawing.Point(0, 108);
            this.accountTypePanel2.Name = "accountTypePanel2";
            this.accountTypePanel2.Size = new System.Drawing.Size(517, 108);
            this.accountTypePanel2.TabIndex = 1;
            this.accountTypePanel2.Visible = false;
            // 
            // accountTypePanel2Debit
            // 
            this.accountTypePanel2Debit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel2Debit.AutoSize = true;
            this.accountTypePanel2Debit.Location = new System.Drawing.Point(469, 36);
            this.accountTypePanel2Debit.Name = "accountTypePanel2Debit";
            this.accountTypePanel2Debit.Size = new System.Drawing.Size(31, 68);
            this.accountTypePanel2Debit.TabIndex = 5;
            // 
            // accountTypePanel2Credit
            // 
            this.accountTypePanel2Credit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel2Credit.AutoSize = true;
            this.accountTypePanel2Credit.Location = new System.Drawing.Point(418, 36);
            this.accountTypePanel2Credit.Name = "accountTypePanel2Credit";
            this.accountTypePanel2Credit.Size = new System.Drawing.Size(31, 69);
            this.accountTypePanel2Credit.TabIndex = 4;
            // 
            // listBox2
            // 
            this.listBox2.AllowDrop = true;
            this.listBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.listBox2.FormattingEnabled = true;
            this.listBox2.ItemHeight = 24;
            this.listBox2.Location = new System.Drawing.Point(3, 36);
            this.listBox2.Margin = new System.Windows.Forms.Padding(10);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(382, 52);
            this.listBox2.TabIndex = 1;
            this.listBox2.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox_DragDrop);
            this.listBox2.DragOver += new System.Windows.Forms.DragEventHandler(this.listBox_DragOver);
            this.listBox2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseDown);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label4.Location = new System.Drawing.Point(466, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Debit";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label5.Location = new System.Drawing.Point(415, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 17);
            this.label5.TabIndex = 2;
            this.label5.Text = "Credit";
            // 
            // accountTypePanel2Label
            // 
            this.accountTypePanel2Label.AutoSize = true;
            this.accountTypePanel2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.accountTypePanel2Label.Location = new System.Drawing.Point(3, 17);
            this.accountTypePanel2Label.Name = "accountTypePanel2Label";
            this.accountTypePanel2Label.Size = new System.Drawing.Size(83, 17);
            this.accountTypePanel2Label.TabIndex = 1;
            this.accountTypePanel2Label.Text = "TypeLabel2";
            // 
            // accountTypePanel1
            // 
            this.accountTypePanel1.AllowDrop = true;
            this.accountTypePanel1.AutoSize = true;
            this.accountTypePanel1.Controls.Add(this.accountTypePanel1Debit);
            this.accountTypePanel1.Controls.Add(this.accountTypePanel1Credit);
            this.accountTypePanel1.Controls.Add(this.listBox1);
            this.accountTypePanel1.Controls.Add(this.label3);
            this.accountTypePanel1.Controls.Add(this.label2);
            this.accountTypePanel1.Controls.Add(this.accountTypePanel1Label);
            this.accountTypePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.accountTypePanel1.Location = new System.Drawing.Point(0, 0);
            this.accountTypePanel1.Name = "accountTypePanel1";
            this.accountTypePanel1.Size = new System.Drawing.Size(517, 108);
            this.accountTypePanel1.TabIndex = 0;
            this.accountTypePanel1.Visible = false;
            // 
            // accountTypePanel1Debit
            // 
            this.accountTypePanel1Debit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel1Debit.AutoSize = true;
            this.accountTypePanel1Debit.Location = new System.Drawing.Point(469, 36);
            this.accountTypePanel1Debit.Name = "accountTypePanel1Debit";
            this.accountTypePanel1Debit.Size = new System.Drawing.Size(31, 68);
            this.accountTypePanel1Debit.TabIndex = 5;
            // 
            // accountTypePanel1Credit
            // 
            this.accountTypePanel1Credit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.accountTypePanel1Credit.AutoSize = true;
            this.accountTypePanel1Credit.Location = new System.Drawing.Point(418, 36);
            this.accountTypePanel1Credit.Name = "accountTypePanel1Credit";
            this.accountTypePanel1Credit.Size = new System.Drawing.Size(31, 69);
            this.accountTypePanel1Credit.TabIndex = 4;
            // 
            // listBox1
            // 
            this.listBox1.AllowDrop = true;
            this.listBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 24;
            this.listBox1.Location = new System.Drawing.Point(3, 36);
            this.listBox1.Margin = new System.Windows.Forms.Padding(10);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(382, 52);
            this.listBox1.Sorted = true;
            this.listBox1.TabIndex = 1;
            this.listBox1.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox_DragDrop);
            this.listBox1.DragOver += new System.Windows.Forms.DragEventHandler(this.listBox_DragOver);
            this.listBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseDown);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label3.Location = new System.Drawing.Point(466, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Debit";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label2.Location = new System.Drawing.Point(415, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Credit";
            // 
            // accountTypePanel1Label
            // 
            this.accountTypePanel1Label.AutoSize = true;
            this.accountTypePanel1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.accountTypePanel1Label.Location = new System.Drawing.Point(3, 17);
            this.accountTypePanel1Label.Name = "accountTypePanel1Label";
            this.accountTypePanel1Label.Size = new System.Drawing.Size(83, 17);
            this.accountTypePanel1Label.TabIndex = 1;
            this.accountTypePanel1Label.Text = "TypeLabel1";
            // 
            // listBox5
            // 
            this.listBox5.AllowDrop = true;
            this.listBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.listBox5.FormattingEnabled = true;
            this.listBox5.ItemHeight = 24;
            this.listBox5.Location = new System.Drawing.Point(69, 10);
            this.listBox5.MinimumSize = new System.Drawing.Size(226, 342);
            this.listBox5.Name = "listBox5";
            this.listBox5.Size = new System.Drawing.Size(448, 484);
            this.listBox5.TabIndex = 5;
            this.listBox5.DragDrop += new System.Windows.Forms.DragEventHandler(this.listBox5_DragDrop);
            this.listBox5.DragOver += new System.Windows.Forms.DragEventHandler(this.listBox_DragOver);
            this.listBox5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox5_MouseDown);
            // 
            // panel14
            // 
            this.panel14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel14.AutoScroll = true;
            this.panel14.Controls.Add(this.listBox5);
            this.panel14.Location = new System.Drawing.Point(671, 27);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(552, 562);
            this.panel14.TabIndex = 6;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(585, 245);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(240, 150);
            this.dataGridView1.TabIndex = 26;
            this.dataGridView1.Visible = false;
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.convertToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // convertToolStripMenuItem
            // 
            this.convertToolStripMenuItem.Name = "convertToolStripMenuItem";
            this.convertToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.convertToolStripMenuItem.Text = "Convert";
            this.convertToolStripMenuItem.Click += new System.EventHandler(this.convertToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // submitToolStripMenuItem1
            // 
            this.submitToolStripMenuItem1.Name = "submitToolStripMenuItem1";
            this.submitToolStripMenuItem1.Size = new System.Drawing.Size(57, 20);
            this.submitToolStripMenuItem1.Text = "Submit";
            this.submitToolStripMenuItem1.Click += new System.EventHandler(this.submitToolStripMenuItem1_Click);
            // 
            // resetToolStripMenuItem1
            // 
            this.resetToolStripMenuItem1.Name = "resetToolStripMenuItem1";
            this.resetToolStripMenuItem1.Size = new System.Drawing.Size(47, 20);
            this.resetToolStripMenuItem1.Text = "Reset";
            this.resetToolStripMenuItem1.Click += new System.EventHandler(this.resetToolStripMenuItem1_Click);
            // 
            // gameObjectiveToolStripMenuItem
            // 
            this.gameObjectiveToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gameObjectiveToolStripMenuItem.Name = "gameObjectiveToolStripMenuItem";
            this.gameObjectiveToolStripMenuItem.Size = new System.Drawing.Size(103, 20);
            this.gameObjectiveToolStripMenuItem.Text = "Game Objective";
            this.gameObjectiveToolStripMenuItem.Click += new System.EventHandler(this.gameObjectiveToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.howToPlayGameToolStripMenuItem,
            this.howToChangeAccountSelectionToolStripMenuItem,
            this.howToStartGameOverToolStripMenuItem,
            this.scoringToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.helpToolStripMenuItem1.ForeColor = System.Drawing.Color.Black;
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem1.Text = "Help";
            // 
            // howToPlayGameToolStripMenuItem
            // 
            this.howToPlayGameToolStripMenuItem.Name = "howToPlayGameToolStripMenuItem";
            this.howToPlayGameToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.howToPlayGameToolStripMenuItem.Text = "How to play";
            this.howToPlayGameToolStripMenuItem.Click += new System.EventHandler(this.howToPlayGameToolStripMenuItem_Click);
            // 
            // howToChangeAccountSelectionToolStripMenuItem
            // 
            this.howToChangeAccountSelectionToolStripMenuItem.Name = "howToChangeAccountSelectionToolStripMenuItem";
            this.howToChangeAccountSelectionToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.howToChangeAccountSelectionToolStripMenuItem.Text = "How to change account selection";
            this.howToChangeAccountSelectionToolStripMenuItem.Click += new System.EventHandler(this.howToChangeAccountSelectionToolStripMenuItem_Click);
            // 
            // howToStartGameOverToolStripMenuItem
            // 
            this.howToStartGameOverToolStripMenuItem.Name = "howToStartGameOverToolStripMenuItem";
            this.howToStartGameOverToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.howToStartGameOverToolStripMenuItem.Text = "How to start game over";
            this.howToStartGameOverToolStripMenuItem.Click += new System.EventHandler(this.howToStartGameOverToolStripMenuItem_Click);
            // 
            // scoringToolStripMenuItem
            // 
            this.scoringToolStripMenuItem.Name = "scoringToolStripMenuItem";
            this.scoringToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.scoringToolStripMenuItem.Text = "Scoring";
            this.scoringToolStripMenuItem.Click += new System.EventHandler(this.scoringToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Enabled = false;
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(46, 20);
            this.toolStripMenuItem2.Text = "00:00";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.submitToolStripMenuItem1,
            this.resetToolStripMenuItem1,
            this.gameObjectiveToolStripMenuItem,
            this.helpToolStripMenuItem1,
            this.toolStripMenuItem2});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1224, 24);
            this.menuStrip1.TabIndex = 25;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1224, 593);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panel14);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Name = "Form1";
            this.Text = "Accounting Game";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.accountTypePanel20.ResumeLayout(false);
            this.accountTypePanel20.PerformLayout();
            this.accountTypePanel19.ResumeLayout(false);
            this.accountTypePanel19.PerformLayout();
            this.accountTypePanel18.ResumeLayout(false);
            this.accountTypePanel18.PerformLayout();
            this.accountTypePanel17.ResumeLayout(false);
            this.accountTypePanel17.PerformLayout();
            this.accountTypePanel16.ResumeLayout(false);
            this.accountTypePanel16.PerformLayout();
            this.accountTypePanel15.ResumeLayout(false);
            this.accountTypePanel15.PerformLayout();
            this.accountTypePanel14.ResumeLayout(false);
            this.accountTypePanel14.PerformLayout();
            this.accountTypePanel13.ResumeLayout(false);
            this.accountTypePanel13.PerformLayout();
            this.accountTypePanel12.ResumeLayout(false);
            this.accountTypePanel12.PerformLayout();
            this.accountTypePanel11.ResumeLayout(false);
            this.accountTypePanel11.PerformLayout();
            this.accountTypePanel10.ResumeLayout(false);
            this.accountTypePanel10.PerformLayout();
            this.accountTypePanel9.ResumeLayout(false);
            this.accountTypePanel9.PerformLayout();
            this.accountTypePanel8.ResumeLayout(false);
            this.accountTypePanel8.PerformLayout();
            this.accountTypePanel7.ResumeLayout(false);
            this.accountTypePanel7.PerformLayout();
            this.accountTypePanel6.ResumeLayout(false);
            this.accountTypePanel6.PerformLayout();
            this.accountTypePanel5.ResumeLayout(false);
            this.accountTypePanel5.PerformLayout();
            this.accountTypePanel4.ResumeLayout(false);
            this.accountTypePanel4.PerformLayout();
            this.accountTypePanel3.ResumeLayout(false);
            this.accountTypePanel3.PerformLayout();
            this.accountTypePanel2.ResumeLayout(false);
            this.accountTypePanel2.PerformLayout();
            this.accountTypePanel1.ResumeLayout(false);
            this.accountTypePanel1.PerformLayout();
            this.panel14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel accountTypePanel19;
        private System.Windows.Forms.Panel accountTypePanel19Debit;
        private System.Windows.Forms.Panel accountTypePanel19Credit;
        private System.Windows.Forms.ListBox listBox20;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label accountTypePanel19Label;
        private System.Windows.Forms.Panel accountTypePanel18;
        private System.Windows.Forms.Panel accountTypePanel18Debit;
        private System.Windows.Forms.Panel accountTypePanel18Credit;
        private System.Windows.Forms.ListBox listBox19;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label accountTypePanel18Label;
        private System.Windows.Forms.Panel accountTypePanel17;
        private System.Windows.Forms.Panel accountTypePanel17Debit;
        private System.Windows.Forms.Panel accountTypePanel17Credit;
        private System.Windows.Forms.ListBox listBox18;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label accountTypePanel17Label;
        private System.Windows.Forms.Panel accountTypePanel16;
        private System.Windows.Forms.Panel accountTypePanel16Debit;
        private System.Windows.Forms.Panel accountTypePanel16Credit;
        private System.Windows.Forms.ListBox listBox17;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label accountTypePanel16Label;
        private System.Windows.Forms.Panel accountTypePanel15;
        private System.Windows.Forms.Panel accountTypePanel15Debit;
        private System.Windows.Forms.Panel accountTypePanel15Credit;
        private System.Windows.Forms.ListBox listBox16;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label accountTypePanel15Label;
        private System.Windows.Forms.Panel accountTypePanel14;
        private System.Windows.Forms.Panel accountTypePanel14Debit;
        private System.Windows.Forms.Panel accountTypePanel14Credit;
        private System.Windows.Forms.ListBox listBox15;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label accountTypePanel14Label;
        private System.Windows.Forms.Panel accountTypePanel13;
        private System.Windows.Forms.Panel accountTypePanel13Debit;
        private System.Windows.Forms.Panel accountTypePanel13Credit;
        private System.Windows.Forms.ListBox listBox14;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label accountTypePanel13Label;
        private System.Windows.Forms.Panel accountTypePanel12;
        private System.Windows.Forms.Panel accountTypePanel12Debit;
        private System.Windows.Forms.Panel accountTypePanel12Credit;
        private System.Windows.Forms.ListBox listBox13;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label accountTypePanel12Label;
        private System.Windows.Forms.Panel accountTypePanel11;
        private System.Windows.Forms.Panel accountTypePanel11Debit;
        private System.Windows.Forms.Panel accountTypePanel11Credit;
        private System.Windows.Forms.ListBox listBox12;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label accountTypePanel11Label;
        private System.Windows.Forms.Panel accountTypePanel10;
        private System.Windows.Forms.Panel accountTypePanel10Debit;
        private System.Windows.Forms.Panel accountTypePanel10Credit;
        private System.Windows.Forms.ListBox listBox11;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label accountTypePanel10Label;
        private System.Windows.Forms.Panel accountTypePanel9;
        private System.Windows.Forms.Panel accountTypePanel9Debit;
        private System.Windows.Forms.Panel accountTypePanel9Credit;
        private System.Windows.Forms.ListBox listBox10;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label accountTypePanel9Label;
        private System.Windows.Forms.Panel accountTypePanel8;
        private System.Windows.Forms.Panel accountTypePanel8Debit;
        private System.Windows.Forms.Panel accountTypePanel8Credit;
        private System.Windows.Forms.ListBox listBox9;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label accountTypePanel8Label;
        private System.Windows.Forms.Panel accountTypePanel7;
        private System.Windows.Forms.Panel accountTypePanel7Debit;
        private System.Windows.Forms.Panel accountTypePanel7Credit;
        private System.Windows.Forms.ListBox listBox8;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label accountTypePanel7Label;
        private System.Windows.Forms.Panel accountTypePanel6;
        private System.Windows.Forms.Panel accountTypePanel6Debit;
        private System.Windows.Forms.Panel accountTypePanel6Credit;
        private System.Windows.Forms.ListBox listBox7;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label accountTypePanel6Label;
        private System.Windows.Forms.Panel accountTypePanel5;
        private System.Windows.Forms.Panel accountTypePanel5Debit;
        private System.Windows.Forms.Panel accountTypePanel5Credit;
        private System.Windows.Forms.ListBox listBox6;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label accountTypePanel5Label;
        private System.Windows.Forms.Panel accountTypePanel4;
        private System.Windows.Forms.Panel accountTypePanel4Debit;
        private System.Windows.Forms.Panel accountTypePanel4Credit;
        private System.Windows.Forms.ListBox listBox4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label accountTypePanel4Label;
        private System.Windows.Forms.Panel accountTypePanel3;
        private System.Windows.Forms.Panel accountTypePanel3Debit;
        private System.Windows.Forms.Panel accountTypePanel3Credit;
        private System.Windows.Forms.ListBox listBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label accountTypePanel3Label;
        private System.Windows.Forms.Panel accountTypePanel2;
        private System.Windows.Forms.Panel accountTypePanel2Debit;
        private System.Windows.Forms.Panel accountTypePanel2Credit;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label accountTypePanel2Label;
        private System.Windows.Forms.Panel accountTypePanel1;
        private System.Windows.Forms.Panel accountTypePanel1Debit;
        private System.Windows.Forms.Panel accountTypePanel1Credit;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label accountTypePanel1Label;
        private System.Windows.Forms.Panel accountTypePanel20;
        private System.Windows.Forms.Panel accountTypePanel20Debit;
        private System.Windows.Forms.Panel accountTypePanel20Credit;
        private System.Windows.Forms.ListBox listBox21;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label accountTypePanel20Label;
        private System.Windows.Forms.ListBox listBox5;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem convertToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem submitToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem gameObjectiveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem howToPlayGameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem howToChangeAccountSelectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem howToStartGameOverToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scoringToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.MenuStrip menuStrip1;
    }
}


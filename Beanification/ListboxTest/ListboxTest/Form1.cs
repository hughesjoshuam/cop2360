﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Configuration;
using System.Reflection;
using System.Text.RegularExpressions;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;



namespace ListboxTest
{
    public partial class Form1 : Form
    {
        private BinaryFormatter formatter = new BinaryFormatter();//Serialization formatter.
        private FileStream stream; //Output file stream.
        List<string> elements = new List<string>();
        List<Account> accounts = new List<Account>();
        public Form1()
        {   
            InitializeComponent();
        }
        //Drag and drop functionality based on code from
        //http://www.codeproject.com/Articles/2006/Drag-and-Drop-between-list-boxes-Beginner-s-Tutori

        //Can be applied to all listboxes
        private void listBox_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
        }

        private void listBox_DragDrop(object sender, DragEventArgs e)
        {
            //variables for the listbox, credit/debit panels, and the panel that contains them
            ListBox lbox = (ListBox)sender;
            Panel answerPanel = (Panel)lbox.Parent;//sets the variable to the container that holds the listbox that tiggered the event handler
            Panel creditPanel = new Panel();
            Panel debitPanel = new Panel();
            string accountType = "";

            //Cycles through the panel and looks for the panels that hold the credit/debit checkboxes and assigns them to the variables
            for(int x = 0; x <= answerPanel.Controls.Count-1; x++)
            {
                if(answerPanel.Controls[x].Name.Equals(answerPanel.Name + "Credit"))
                {
                    creditPanel = (Panel)answerPanel.Controls[x];
                }
                if (answerPanel.Controls[x].Name.Equals(answerPanel.Name + "Debit")) 
                {
                    debitPanel = (Panel)answerPanel.Controls[x];
                }
                if(answerPanel.Controls[x].Name.Equals(answerPanel.Name + "Label"))
                {
                    accountType = answerPanel.Controls[x].Text;
                }
            }

            if (e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                string str = (string)e.Data.GetData(DataFormats.StringFormat);
                //Creats the two checkboxes
                CheckBox debitBox = new CheckBox();
                CheckBox creditBox = new CheckBox();

                //Sets the names according to the item that is being dropped
                debitBox.Name = str + "Debit";
                creditBox.Name = str + "Credit";

                debitBox.CheckStateChanged += debitBox_CheckStateChanged;
                creditBox.CheckStateChanged += creditBox_CheckStateChanged;

                debitBox.Dock = DockStyle.Top;
                creditBox.Dock = DockStyle.Top;

                //variable to hold the account
                Account playerAccount = new Account(0);

                foreach(Account item in accounts)
                {
                    if(item.trimmedName.Equals(str))
                    {
                        playerAccount = item;
                    }
                }

                lbox.Items.Add(str);
                creditPanel.Controls.Add(creditBox);//adds the checkboxes into the corresponding listboxes on the form
                debitPanel.Controls.Add(debitBox);

                playerAccount.playerElement = accountType;

                playerAccount.playerIsCredit = false;
                playerAccount.playerIsDebit = false;

                //Sorts the checkboxes 
                for (int itemIndex = 0; itemIndex <= lbox.Items.Count - 1; itemIndex++)
                {
                    for (int boxIndex = 0; boxIndex <= lbox.Items.Count - 1; boxIndex++)
                    {
                        if (creditPanel.Controls[boxIndex].Name.Equals(lbox.Items[itemIndex].ToString() + "Credit"))
                        {
                            creditPanel.Controls[boxIndex].BringToFront();
                        }

                        if (debitPanel.Controls[boxIndex].Name.Equals(lbox.Items[itemIndex].ToString() + "Debit"))
                        {
                            debitPanel.Controls[boxIndex].BringToFront();
                        }
                    }
                }
            }
            lbox.Height = lbox.PreferredHeight;           
        }

        private void listBox_MouseDown(object sender, MouseEventArgs e)
        {
            ListBox lbox = (ListBox)sender;
            Panel answerPanel = (Panel)lbox.Parent;
            Panel creditPanel = new Panel();
            Panel debitPanel = new Panel();

            for (int x = 0; x <= answerPanel.Controls.Count - 1; x++)
            {
                if (answerPanel.Controls[x].Name.Equals(answerPanel.Name + "Credit"))
                {
                    creditPanel = (Panel)answerPanel.Controls[x];
                }
                if (answerPanel.Controls[x].Name.Equals(answerPanel.Name + "Debit"))
                {
                    debitPanel = (Panel)answerPanel.Controls[x];
                }
            }            
            
            if (lbox.Items.Count == 0)
                return;

            int item = lbox.IndexFromPoint(e.X, e.Y);
            string name = lbox.Items[item].ToString();
            DragDropEffects effect1 = DoDragDrop(name, DragDropEffects.All);

            if (effect1 == DragDropEffects.All)
            {
                int index = lbox.IndexFromPoint(e.X, e.Y);

                //Removes the checkboxes from the panel and releases resources
                //based on code from http://stackoverflow.com/questions/2014286/removing-dynamically-created-controls-c-sharp
                for (int controlIndex = 0; controlIndex <= creditPanel.Controls.Count - 1; controlIndex++ )
                {
                    if (creditPanel.Controls[controlIndex].Name.Equals(lbox.Items[index].ToString() + "Credit"))
                    {
                        creditPanel.Controls[controlIndex].Dispose();
                    }
                }
                for (int controlIndex = 0; controlIndex <= debitPanel.Controls.Count - 1; controlIndex++ )
                {
                    if (debitPanel.Controls[controlIndex].Name.Equals(lbox.Items[index].ToString() + "Debit")) 
                    {
                        debitPanel.Controls[controlIndex].Dispose();
                    }
                }
                    lbox.Items.RemoveAt(index);

            }

            lbox.Height = lbox.PreferredHeight;
        }


        private void listBox5_MouseDown(object sender, MouseEventArgs e)
        {
            if (listBox5.Items.Count == 0)
                return;

            int index = listBox5.IndexFromPoint(e.X, e.Y);//variable to validate
            if (index >= 0)//prevents a negative integer from being the items index
            {
                string name = listBox5.Items[index].ToString();
                DragDropEffects effect1 = DoDragDrop(name, DragDropEffects.All);
                if (effect1 == DragDropEffects.All)
                {
                    listBox5.Items.RemoveAt(index);
                }
            }

        }


        private void listBox5_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                string str = (string)e.Data.GetData(DataFormats.StringFormat);
                Account playerAccount = new Account(0);
                listBox5.Items.Add(str);

                foreach(Account item in accounts)
                {
                    if (item.trimmedName.Equals(str))
                    {
                        playerAccount = item;
                    }
                }

                if(!string.IsNullOrEmpty(playerAccount.playerElement))
                {
                    playerAccount.playerElement = null;
                    playerAccount.playerIsCredit = false;
                    playerAccount.playerIsDebit = false;
                }
            }
        }

        void creditBox_CheckStateChanged(object sender, EventArgs e)
        {
            CheckBox box = (CheckBox)sender;
            string accountName = box.Name.Replace("Credit", "");
            Account playerAccount = new Account(0);

            foreach(Account item in accounts)
            {
                if(item.trimmedName.Equals(accountName))
                {
                    playerAccount = item;
                }
            }

            if (box.Checked)
            {
                playerAccount.playerIsCredit = true;
            }
            else
            {
                playerAccount.playerIsCredit = false;
            }

           
        }

        private void debitBox_CheckStateChanged(object sender, EventArgs e)
        {
            CheckBox box = (CheckBox)sender;
            string accountName = box.Name.Replace("Debit","");
            Account playerAccount = new Account(0);

            foreach (Account item in accounts)
            {
                if (item.trimmedName.Equals(accountName))
                {
                    playerAccount = item;
                }
            }

            if (box.Checked)
            {
                playerAccount.playerIsDebit = true;
            }
            else
            {
                playerAccount.playerIsDebit = false;
            }

           

        }

        private void resetToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            foreach (Panel elementPanel in panel1.Controls)
            {
                for (int x = 0; x <= elementPanel.Controls.Count - 1; x++)
                {
                    if (elementPanel.Controls[x].Name.Equals(elementPanel.Name + "Credit") || elementPanel.Controls[x].Name.Equals(elementPanel.Name + "Debit"))
                    {
                        Panel boxPanel = (Panel)elementPanel.Controls[x];
                        boxPanel.Controls.Clear();
                    }
                    else if (elementPanel.Controls[x] is ListBox)
                    {
                        ListBox lbox = (ListBox)elementPanel.Controls[x];
                        lbox.Items.Clear();
                        lbox.Height = lbox.PreferredHeight;
                    }
                }

            }
            listBox5.Items.Clear();
            for (int x = 0; x <= accounts.Count - 1; x++)
            {
                listBox5.Items.Add(accounts[x].trimmedName);
            }
        }
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This game was created for the Accounting Class by C# Programming II class." +
     " Professor James Barrell 2016 Spring Semester");
        }

        private void gameObjectiveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("The purpose of this game is to test the ability to match the account with correct chart of account type." + " The next step is to select if the account is a credit or debit.");
        
        }

        private void howToPlayGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("To play the game click and drag account from list located to right of the screen to appropriate account type to the left of list.");
        }

        private void howToChangeAccountSelectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Click on account and drag to appropriate account type." + " Utilizing the drag and drop method the account can also be returned to the original list.");
        }

        private void howToStartGameOverToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Pressing the reset button will clear all selections from account type and return to list of accounts." +
                  " Reseting the timer and allowing you to play the game again.");
        }

        private void scoringToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(" Each solution has two correct answers." + " One point is awarded for each correct answer." +
                         " One point for selecting correct account type and one point for correct debit or credit.");
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result;
            string fileName = null;

            using (OpenFileDialog xslDialog = new OpenFileDialog())
            {
                xslDialog.Filter = "Bean Files (.bean)|*.bean";//unique file extension for game files so only those files will show as options to load
                xslDialog.FilterIndex = 1;
                result = xslDialog.ShowDialog();
                fileName = xslDialog.FileName;
            }



            if (!string.IsNullOrWhiteSpace(fileName))
            {
                //clears gui on load of new game
                accounts.Clear();
                elements.Clear();
                listBox5.Items.Clear();
                foreach (Panel control in panel1.Controls)
                {
                    control.Visible = false;
                }



                stream = new FileStream(fileName, FileMode.Open, FileAccess.Read);

                do
                {
                    Account account = (Account)formatter.Deserialize(stream); //This will automagically grab the next account.
                    accounts.Add(account); //This adds the account object to the collection.
                } while (!stream.Position.Equals(stream.Length)); //If the stream is not at the EOF, it will continue to loop.

                foreach (Account item in accounts)//adds the account's element if not already contained in the list
                {
                    if (!elements.Contains(item.element))
                    {
                        elements.Add(item.element);
                    }
                }

                stream.Close();
            }
      
            //randomizes the keys of the loaded accounts
            foreach(Account item in accounts)
            {
                item.randomizeKey();
            }


            //Sets number of visible panels equal to the number of account-types imported
            for (int controlIndex = 0; controlIndex <= panel1.Controls.Count - 1; controlIndex++)
            {
                Panel typePanel = (Panel)panel1.Controls[controlIndex];
                string item = null;
                if (controlIndex >= 20 - elements.Count)
                {
                    typePanel.Visible = true;


                    foreach (string type in elements)
                    {
                        if (elements.IndexOf(type) == 19 - controlIndex)
                        {
                            item = type;
                        }
                    }

                    foreach (Control controlItem in typePanel.Controls)
                    {
                        if (controlItem.Name.Equals(typePanel.Name + "Label"))
                        {
                            controlItem.Text = item;
                        }
                    }
                }
            }
            //Sets number of visible panels equal to the number of account-types imported
            for (int controlIndex = 0; controlIndex <= panel1.Controls.Count - 1; controlIndex++)
            {
                Panel typePanel = (Panel)panel1.Controls[controlIndex];
                string item = null;
                if (controlIndex >= 20 - elements.Count)
                {
                    typePanel.Visible = true;


                    foreach (string type in elements)
                    {
                        if (elements.IndexOf(type) == 19 - controlIndex)
                        {
                            item = type;
                        }
                    }

                    foreach (Control controlItem in typePanel.Controls)
                    {
                        if (controlItem.Name.Equals(typePanel.Name + "Label"))
                        {
                            controlItem.Text = item;
                        }
                    }
                }
            }

            accounts.Sort((account1, account2) => account1.randomKey.CompareTo(account2.randomKey));//sorts by randomKey

            for (int count = 0; count <= accounts.Count - 1; count++)
            {
                //Imports the trimmed name into the listbox
                listBox5.Items.Add(accounts[count].trimmedName);
            }
            //Sizes the answer listbox
            if (listBox5.Height > 226)
            {
                listBox5.Height = listBox5.PreferredHeight;
            }


        }

        private void submitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            int playerScore = 0;
            string points = "";
            foreach(Account item in accounts)
            {
                playerScore += item.score();
            }

            if (playerScore != 1)
            {
                points = " points!";
            }
            else
            {
                points = " point!";
            }

            MessageBox.Show("You scored " + Convert.ToString(playerScore) + points, "Score", MessageBoxButtons.OK);
        }

        private void convertToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Set Excel connection Currently at client on campus
            DialogResult result;
            string fileName = null;
            string beanFileName = null;

            using (OpenFileDialog xslDialog = new OpenFileDialog())
            {
                xslDialog.Filter = "Excel file (.xlsx)|*.xlsx";
                xslDialog.FilterIndex = 1;
                result = xslDialog.ShowDialog();
                fileName = xslDialog.FileName;
            }
            string gameDataConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties=\'Excel 12.0;HDR=Yes'";

            if (!string.IsNullOrWhiteSpace(fileName))// runs only if a file is selected
            {
                using (SaveFileDialog convertDialog = new SaveFileDialog())
                {
                    convertDialog.Title = "Convert file";
                    convertDialog.Filter = "Bean file (.bean)|*.bean";
                    result = convertDialog.ShowDialog();
                    beanFileName = convertDialog.FileName;

                }
                if (!string.IsNullOrWhiteSpace(beanFileName))//runs only if a file is "saved"
                {
                    stream = new FileStream(beanFileName, FileMode.OpenOrCreate, FileAccess.Write);

                    // Create the dataset
                    DataSet gameDataSet = new DataSet();
                    DataTable gameDataTable = new DataTable();
                    OleDbConnection gameDataSheet = null;
                    try
                    {
                        gameDataSheet = new OleDbConnection(gameDataConn);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: Failed to create a database connection. \n{0}", ex.Message);
                        return;
                    }
                    try
                    {
                        string gameDataList = "SELECT * FROM [Sheet1$]";
                        OleDbCommand myCommand = new OleDbCommand(gameDataList, gameDataSheet);
                        OleDbDataAdapter adapter = new OleDbDataAdapter(myCommand);

                        gameDataSheet.Open();
                        // OleDbDataReader myReader = myCommand.ExecuteReader(); //The code seems to work fine without this line...
                        adapter.Fill(gameDataTable);

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: Failed to retrieve the required data from the DataBase.\n{0}", ex.Message);
                        return;
                    }
                    finally
                    {
                        gameDataSheet.Close();
                    }

                    // Fill datagridview
                    dataGridView1.DataSource = gameDataTable;

                    List<object> gameDatalst = new List<object>(); //I assume this is the list for the objects to go into.
                    string element = "Default"; //This needs to be initialized or else it won't compile.
                    string cellZeroValue = "Default"; //I might need a variable to store the value of the current cell to compare boolean values.
                    Random rng = new Random();
                    Regex elementFormat = new Regex(@".*(\d000-\d999)"); //Regex expressions to be compared. This one consists of a word, followed by a space, and a 4 digit number.
                    Regex accountFormat = new Regex(@"\d\d\d\d-.*"); //This expression consists of 4 digits, followed by a dash, and then a word.

                    for (int y = 0; y < dataGridView1.RowCount - 1; y++) //This is where I'm testing the loop to add each object to the list.
                    {
                        if (!dataGridView1[0, y].Value.Equals(null))
                        {
                            cellZeroValue = dataGridView1[0, y].Value.ToString();
                            bool isElement = elementFormat.Match(cellZeroValue).Success;
                            bool isAccount = accountFormat.Match(cellZeroValue).Success;
                            if (isElement == true)
                                element = dataGridView1[0, y].Value.ToString(); //If the value in column 0 of the current row matches the element format, it changes the value of element to the value in that cell.
                            else if (isAccount == true)
                            {
                                Account account = new Account(y); //If the value in column 0 of the current cell matches account format, then it will create a new object account with the ID of the current row.
                                //account.randomizeKey(); //This assigns a random key to put the entry in a random place.
                                account.Element = element; //This sets the element of the account to the element defined earlier.
                                account.AccountName = cellZeroValue; //This sets the name of the account to the name in the data grid view.
                                account.Trim(account.AccountName); //This sets the trimmedName propery of account to the 3 digit equivalent of the account name.
                                if (dataGridView1[1, y].Value.Equals("Debit"))
                                    account.isDebit = true; //Sets isDebit to true if it finds Debit in the column.
                                if (dataGridView1[2, y].Value.Equals("Credit"))
                                    account.isCredit = true;
                                //gameDatalst.Add(account);
                                formatter.Serialize(stream, account);
                            }
                        }
                    }
                    stream.Close(); //Closes the stream after adding objects to the list.
                }               
            }           
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }
    }

}
